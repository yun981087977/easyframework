$(function () {
  var params = $.getUrlParams();
  $.doAjax("system/getMenu", {
    id: params['id']
  }, function (menu) {
    $("#pid").val(menu['id']);
    $("#menuId").val(menu['menuId']);
    $("#menuName").val(menu['menuName']);
    $("#parentId").val(menu['parentId']).attr("data-value", menu['parentId']);
    $("#menuLevel").val(menu['menuLevel']);
    $("#menuIcon").val(menu['menuIcon']);
    $("#menuLink").val(menu['menuLink']);
    $("input[name='menuStatus'][value=" + menu['menuStatus'] + "]").attr("checked", true);
  });
});