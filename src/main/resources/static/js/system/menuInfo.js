$(function () {
  var params = $.getUrlParams();
  $.doAjax("system/getMenu", params, function (menu) {
    $("#menuId").text(menu['id']);
    $("#menuName").text(menu['menuName']);
    $("#parentId").text(menu['parentId']);
    $("#menuLevel").text(menu['menuLevel']);
    $("#menuIcon").addClass(menu['menuIcon']);
    $("#menuLink").text(menu['menuLink']);
    $("#menuStatus").text(menu['menuStatusStr']);
    $("#menuSort").text(menu['menuSort']);
  });
});