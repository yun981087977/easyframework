$(function () {
  var params = $.getUrlParams();
  if (params.type && params.type === "edit") {
    $.doAjax("system/getMenu", params, function (menu) {
      $("#pid").val(menu['pid']);
      $("#menuId").val(menu['menuId']);
      $("#menuName").val(menu['menuName']);
      $("#parentId").val(menu['parentId']).attr("data-value", menu['parentId']);
      $("#menuLevel").val(menu['menuLevel']);
      $("#menuIcon").val(menu['menuIcon']);
      $("#menuLink").val(menu['menuLink']);
      $("input[name='menuStatus'][value=" + menu['menuStatus'] + "]").attr("checked", true);
    });
  }
  $("#orgId").on('change', function () {
    var orgId = $("#orgId").val();
    if (orgId != null && orgId != "") {
      $.ajax({
        url: "role/listRole",
        dataType: "json",
        type: "post",
        data: {
          "orgId": orgId
        },
        success: function (data) {
          var datlist = data.data;
          $("#roleId").html("");
          var option = "";
          for (var i = 0; i < datlist.length; i++) {
            option += "<option value=" + datlist[i].roleId + ">" + datlist[i].roleName + "</option>"
          }
          $("#roleId").html(option);
        }
      })
    } else {
      $("#roleId").html("<option value=''>请选择角色</option>");
    }
  });
});