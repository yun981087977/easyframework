$(function () {
  var params = $.getUrlParams();
  $.doAjax("system/getMenu", params, function (menu) {
    $("#menuId").text(menu['menuId']);
    $("#menuName").text(menu['menuName']);
    $("#parentId").text(menu['parentId']);
    $("#menuLevel").text(menu['menuLevel']);
    $("#menuIcon").addClass(menu['menuIcon']);
    $("#menuLink").text(menu['menuLink']);
    $("#menuStatus").text(menu['menuStatusStr']);
  });
});