/**
 * Created by wuxue on 2017/7/6.
 */
$(function () {
  $("#prisonerList").gridTable({
    url: "prisonerAccount/pagePrisonerAccount",
    columns: [{
        title: "犯人编号",
        field: "id"
      },
      {
        title: "犯人姓名",
        field: "prisonerName"
      },
      {
        title: "监区",
        field: "room"
      },
      {
        title: "监仓",
        field: "room"
      },
      {
        title: "监室",
        field: "room"
      },
      {
        title: "账户",
        field: "bankCard"
      },
      {
        title: "开户行",
        field: "bankName"
      },
      {
        title: "余额",
        field: "balance",
        dataType: "decimal"
      },
      {
        title: "冻结金额",
        field: "freezingAmount",
        dataType: "decimal"
      },
      {
        title: "可用金额",
        field: "availableAmount",
        dataType: "decimal"
      },
      {
        title: "创建时间",
        field: "createTime"
      }
    ],
    idField: "id",
    infoUrl: "account/prisonerInfo.html",
    delUrl: "goods/delGoods",
    controls: {
      del: {
        text: "销户"
      },
      edit: {
        show: false
      }
    }
  });
});