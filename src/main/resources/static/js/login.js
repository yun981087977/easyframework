$(function () {
  //判断页面地址进行跳转
  var pathname = top.window.location.pathname;
  if (!("/login.html" === pathname)) {
    top.window.location.href = top.window.location.origin + "/login.html";
  }

  var $username = $("#username");
  var $password = $("#password");
  var $patchca = $("#patchca");
  $username.val(store.get("username"));
  $password.val(store.get("password"));

  // 刷新验证码
  var refreshPatchca = function () {
    $("#patchcaImg").hide().attr('src', 'patchca.img?time=' + new Date().getTime()).fadeIn();
  };
  $("#patchcaImg").on("click", refreshPatchca);

  //登录
  var login = function () {
    var username = $username.val();
    if (username.length === 0) {
      $username.parents(".form-group").removeClass("has-success").addClass("has-error").find(".help-block").text("用户名不能为空");
      $username.focus();
      return;
    } else {
      $username.parents(".form-group").removeClass("has-error").addClass("has-success").find(".help-block").text("");
      store.set("username", username);
    }
    var password = $password.val();
    if (password.length === 0) {
      $password.parents(".form-group").removeClass("has-success").addClass("has-error").find(".help-block").text("密码不能为空");
      $password.focus();
      return;
    } else {
      $password.parents(".form-group").removeClass("has-error").addClass("has-success").find(".help-block").text("");
      store.set("password", password);
    }
    var patchca = $patchca.val();
    if (patchca) {
      if (patchca.length === 0) {
        $patchca.parents(".form-group").removeClass("has-success").addClass("has-error").find(".help-block").text("验证码不能为空");
        $patchca.focus();
        return;
      } else {
        $patchca.parents(".form-group").removeClass("has-error").addClass("has-success").find(".help-block").text("");
      }
    }

    $("#login").off("click").html("<span class='fa fa-spinner fa-pulse'></span> 正在提交").addClass("disabled");

    var $form = $("#loginForm");
    $.doAjax("auth/login", $form.serialize(), function () {
      // 登录成功
      window.location.href = baseUrl;
    }, function (error) {
      if (error.code === errorCode.PATCHCA_MISTAKE) {
        $patchca.parents(".form-group").removeClass("has-success").addClass("has-error").find(".help-block").text(error.msg);
        refreshPatchca();
        $patchca.val("");
        $patchca.focus();
      } else if (error.code === errorCode.USERNAME_NOT_EXIST) {
        $username.parents(".form-group").removeClass("has-success").addClass("has-error").find(".help-block").text(error.msg);
        $username.val("");
        store.remove("username");
        $username.focus();
      } else if (error.code === errorCode.PASSWORD_MISTAKE) {
        $password.parents(".form-group").removeClass("has-success").addClass("has-error").find(".help-block").text(error.msg);
        $password.val("");
        store.remove("password");
        $password.focus();
      }
      $("#login").on("click", login).html("提交").removeClass("disabled");
    });
  };

  $("#login").on("click", login);
  //回车动作
  $(document).bind("keydown", function (event) {
    var code = event.keyCode || event.which;
    if (code === 13) {
      if ($("#login").hasClass("disabled")) {
        return false;
      } else {
        login();
      }
    }
  });

  $("#cancel").on("click", function () {
    $username.val("");
    $password.val("");
  });
});