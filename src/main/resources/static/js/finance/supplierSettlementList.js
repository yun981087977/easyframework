// 外网民警 - 生活科结算 
$(function () {
  $("#supplierSettlementList").gridTable({
    url: "settlement/pageSettlement2",
    columns: [{
        title: "结算单号",
        field: "id",
        format: Num
      },
      {
        title: "供应商",
        field: "supplierName"
      },
      {
        title: "结算金额",
        field: "totalSettlement",
        dataType: "decimal"
      },
      {
        title: "开户行",
        field: "bankName"
      },
      {
        title: "银行账户",
        field: "bankCard"
      },
      {
        title: "结算单状态",
        field: "statusStr"
      },
      {
        title: "结算单详情",
        field: "",
        format: showDetails
      },
      {
        title: "操作",
        field: "status",
        format: btnStyle
      }
    ],
    idField: "id",
    infoUrl: "finance/supplierSettlementInfo.html",
    showControl: false,
    showDetail: false,
    showCheckbox: false,
    multiSelect: false, // 是否可以多选
    onClickRow: null, // 行单击事件回调
    onDoubleClickRow: null, // 行双击事件回调
  });
  detail();
});

function showDetails() {
  return '<span class="label label-look info Info" id="Info" data-click="lookDetail(this)"><a href="javascript:void(0)">[查看]</a></span>'
}

// 修改文字内容
function detail() {
  $('.showdetail').text('结算单详情')
}

function Num(id) {
  return "<span class='Number'>" + id + "</span>";
}

// 自定义详情
function lookDetail(e) {
  var self_id = $(e).parent().parent().find('.Number').text();
  $.openIframeDialog({
    url: 'finance/supplierSettlementInfo.html?id=' + self_id,
    title: '结算单详情',
    buttons: {
      ok: {
        show: false,
      },
      cancel: {
        show: false,
      }
    }
  });
}

//判断状态
function toStatus(status) {
  switch (status) {
    case '2':
      return "<span class='label label-success'>已审核</span>";
    case '1':
      return "<span class='label label-default'>待审核</span>";
    case '0':
      return "<span class='label label-danger'>审核未通过</span>"
    default:
      return status;
  }
}

// 商品操作栏内容
function btnStyle(status) {
  switch (status) {
    case '0':
      return '<span data-menu-id="030101" data-click="audit(this);" class="selfBtnControl">[提交申请]</span>';
    case '1':
      return '<span data-menu-id="030102" data-click="auditSelectRow(this);" class="selfBtnControl">[确认审核并申请发票]</span>';
    case '20':
      return '<span data-menu-id="030105" data-click="send(this);" class="selfBtnControl">[寄送发票]</span>';
    case '30':
      return '<span data-menu-id="030106" data-click="receive(this);" class="selfBtnControl">[收到发票]</span>';
    case '40':
      return '<span data-menu-id="030107" data-click="playMoney(this);" class="selfBtnControl">[确认已打款]</span>';
    case '50':
      return '';
    default:
      return status;
  }
}

//提交审核
function audit(e) {
  var gridTableList = $$selection.get("gridTable", "supplierSettlementList");
  var selectedRows = gridTableList.getSelectedRows();
  var self_id = $(e).parent().parent().find('.Number').text();
  $.confirm({
    title: "审核确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认要提交审核此结算单？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确认",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          arr.push(self_id);
          var idStr = arr.join(",");
          var scb = function () {
            $.success("结算单审核申请已提交!");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("settlement/submitAnAudit", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//审核通过
function auditSelectRow(e) {
  var gridTableList = $$selection.get("gridTable", "supplierSettlementList");
  var selectedRows = gridTableList.getSelectedRows();
  var self_id = $(e).parent().parent().find('.Number').text();
  $.confirm({
    title: "审核通过",
    icon: "fa fa-question-circle-o",
    content: "<p>确认通过该商品的审核并申请商品发票吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          arr.push(self_id);
          var idStr = arr.join(",");
          var scb = function () {
            $.success("审核通过成功");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("settlement/failureAudit", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//审核不通过
function unAuditSelectRow(e) {
  var gridTableList = $$selection.get("gridTable", "supplierSettlementList");
  var selectedRows = gridTableList.getSelectedRows();
  var self_id = $(e).parent().parent().find('.Number').text();
  $.confirm({
    title: "审核不通过",
    icon: "fa fa-question-circle-o",
    content: "<p>确认不通过该商品的审核吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          arr.push(self_id);
          var idStr = arr.join(",");
          var scb = function () {
            $.success("审核不通过!");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("settlement/passAudit", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//申请发票
function apply(e) {
  var gridTableList = $$selection.get("gridTable", "supplierSettlementList");
  var selectedRows = gridTableList.getSelectedRows();
  var self_id = $(e).parent().parent().find('.Number').text();
  $.confirm({
    title: "申请发票",
    icon: "fa fa-question-circle-o",
    content: "<p>确认申请该商品的发票？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          arr.push(self_id);
          var idStr = arr.join(",");
          var scb = function () {
            $.success("申请发票成功!");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("settlement/applyForInvoice", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//寄送发票
function send(e) {
  var gridTableList = $$selection.get("gridTable", "supplierSettlementList");
  var selectedRows = gridTableList.getSelectedRows();
  var self_id = $(e).parent().parent().find('.Number').text();
  $.confirm({
    title: "寄送发票",
    icon: "fa fa-question-circle-o",
    content: "<p>确认寄送该商品的发票吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          arr.push(self_id);
          var idStr = arr.join(",");
          var scb = function () {
            $.success("寄送发票成功!");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("settlement/sendInvoice", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//收取发票
function receive(e) {
  var gridTableList = $$selection.get("gridTable", "supplierSettlementList");
  var selectedRows = gridTableList.getSelectedRows();
  var self_id = $(e).parent().parent().find('.Number').text();
  $.confirm({
    title: "收取发票",
    icon: "fa fa-question-circle-o",
    content: "<p>确认收到了该商品的发票吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          arr.push(self_id);
          var idStr = arr.join(",");
          var scb = function () {
            $.success("收取发票成功");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("settlement/receiveInvoice", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//确认已打款
function playMoney(e) {
  var gridTableList = $$selection.get("gridTable", "supplierSettlementList");
  var selectedRows = gridTableList.getSelectedRows();
  var self_id = $(e).parent().parent().find('.Number').text();
  $.confirm({
    title: "确认已打款",
    icon: "fa fa-question-circle-o",
    content: "<p>确认收到了该商品的转账吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          arr.push(self_id);
          var idStr = arr.join(",");
          var scb = function () {
            $.success("已打款成功!");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("settlement/playMoney", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}