/**
 * Created by wuxue on 2017/7/6.
 */
$(function () {
  var param = $.getUrlParams();
  $("#prisonerSettlementInfo").gridTable({
    url: "orderPrisoner/listOrderPrisoner",
    columns: [{
        title: "订单号",
        field: "id"
      },
      {
        title: "犯人姓名",
        field: "prisonerName"
      },
      {
        title: "监仓号",
        field: "room"
      },
      {
        title: "已发数量",
        field: "totalInvoice",
        dataType: "number"
      },
      {
        title: "商品总数量",
        field: "totalCount",
        dataType: "number"
      },
      {
        title: "订单总额",
        field: "totalMoney",
        dataType: "decimal"
      },
      {
        title: "创建时间",
        field: "createTime"
      }
    ],
    params: {
      id: param.orderPrisonerId
    },
    showCheckbox: false,
    showControl: false,
    onDoubleClickRow: null,
    idField: "id",
    infoUrl: "finance/prisonerSettlementInfo.html",
    controls: {
      edit: {
        show: false
      },
      del: {
        show: false
      }
    }
  });
});
//判断状态
function toStatus(status) {
  switch (status) {
    case '2':
      return "<span class='label label-orange'>已审核</span>";
    case '1':
      return "<span class='label label-default'>待审核</span>";
    case '4':
      return "<span class='label label-yellow'>配送中</span>";
    case '5':
      return "<span class='label label-warning'>部分收货</span>";
    case '9':
      return "<span class='label label-success'>全部收货</span>";
    case '10':
      return "<span class='label label-danger'>审核未通过</span>";
    default:
      return status;
  }
}