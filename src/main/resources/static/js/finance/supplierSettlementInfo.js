/**
 * Created by wuxue on 2017/7/6.
 */
$(function () {
  var param = $.getUrlParams();
  $("#supplierSettlementList").gridTable({
    url: "orderSupplier/getDetailList",
    columns: [{
        title: "订单号",
        field: "id"
      },
      {
        title: "商品数量",
        field: "totalCount",
        dataType: "number"
      },
      {
        title: "发货数量",
        field: "totalDeliver",
        dataType: "number"
      },
      {
        title: "监区",
        field: "area"
      },
      {
        title: "下单日期",
        field: "createTime"
      },
      {
        title: "收货日期",
        field: "updateTime"
      },
      {
        title: "订单金额",
        field: "totalSettlement",
        dataType: "decimal"
      },
    ],
    params: {
      "id": param.id
    },
    showCheckbox: false,
    showControl: false,
    showDetail: false,
    showSerialNumber: false, // 是否显示序号
    onDoubleClickRow: null
  });
});

$(function () {
  var params = $.getUrlParams();
  $.doAjax("settlement/getSettlement", params, function (settlement) {
    $("#id").text(settlement['id']);
    $("#prisonsName").text(settlement['prisonsName']);
    $("#supplierName").text(settlement['supplierName']);
    $("#totalSettlement").text("￥" + (Number(settlement['totalSettlement']) / 100).toFixed(2));
    $("#createTime").text(settlement['createTime']);
    $("#statusStr").text(settlement['statusStr']);
    $("#totalSettlementCount").text("￥" + (Number(settlement['totalSettlement']) / 100).toFixed(2));
    $("#createUserName").text(settlement['createName']);
    $("#submitUserName").text(settlement['submitName']);
    $("#auditUserName").text(settlement['auditName']);
  });
});