// 外网监狱监所端: 监狱监所端结算单查询
$(function () {
  $("#supplierSettlementList").gridTable({
    url: "settlement/pageSettlement",
    columns: [{
        title: "结算单号",
        field: "id"
      },
      {
        title: "供应商",
        field: "supplierName"
      },
      {
        title: "结算金额",
        field: "totalSettlement",
        dataType: "decimal"
      },
      {
        title: "户名",
        field: "bankName"
      },
      {
        title: "银行账户",
        field: "bankCard"
      },
      {
        title: "结算单状态",
        field: "statusStr"
      },
      {
        title: "结算时间",
        field: "createTime"
      }
    ],
    idField: "id",
    infoUrl: "finance/supplierSettlementInfo.html",
    showControl: false,
    showCheckbox: false, // 是否显示复选框
    multiSelect: false, // 是否可以多选
    onClickRow: null, // 行单击事件回调
    onDoubleClickRow: null, // 行双击事件回调
  });
  changeWord();
});

function changeWord() {
  $('.showdetail').text('结算单详情');
}
//判断状态
function toStatus(status) {
  switch (status) {
    case '2':
      return "<span class='label label-success'>已审核</span>";
    case '1':
      return "<span class='label label-default'>待审核</span>";
    case '0':
      return "<span class='label label-danger'>审核未通过</span>"
    default:
      return status;
  }
}

//提交审核
function audit() {
  var gridTableList = $$selection.get("gridTable", "supplierSettlementList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "审核确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认审核选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条数据吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "审核",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("审核通过：通过了" + selectedRows.length + "条数据");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("settlement/submitAnAudit", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//审核通过
function auditSelectRow() {
  var gridTableList = $$selection.get("gridTable", "supplierSettlementList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "审核通过",
    icon: "fa fa-question-circle-o",
    content: "<p>确认通过选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条数据吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("审核通过：通过" + selectedRows.length + "条数据");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("settlement/failureAudit", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//审核不通过
function unAuditSelectRow() {
  var gridTableList = $$selection.get("gridTable", "supplierSettlementList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "审核不通过",
    icon: "fa fa-question-circle-o",
    content: "<p>确认不通过选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条数据吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("审核不通过：不通过" + selectedRows.length + "条数据");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("settlement/passAudit", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//申请发票
function apply() {
  var gridTableList = $$selection.get("gridTable", "supplierSettlementList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "申请发票",
    icon: "fa fa-question-circle-o",
    content: "<p>确认申请选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条数据吗的发票？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("申请发票：申请了" + selectedRows.length + "条数据的发票");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("settlement/applyForInvoice", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//寄送发票
function send() {
  var gridTableList = $$selection.get("gridTable", "supplierSettlementList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "寄送发票",
    icon: "fa fa-question-circle-o",
    content: "<p>确认寄送选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条数据的发票吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("寄送发票：寄送了" + selectedRows.length + "条数据的发票");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("settlement/sendInvoice", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//收取发票
function receive() {
  var gridTableList = $$selection.get("gridTable", "supplierSettlementList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "收取发票",
    icon: "fa fa-question-circle-o",
    content: "<p>确认收到了选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条数据的发票吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("收取发票：收到了" + selectedRows.length + "条数据的发票");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("settlement/receiveInvoice", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}