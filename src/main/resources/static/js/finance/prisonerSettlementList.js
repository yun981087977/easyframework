/**
 * Created by wuxue on 2017/7/25.
 */
$(function () {
  $("#prisonerSettlementList").gridTable({
    url: "prisonerSettlement/pagePrisonerSettlement",
    columns: [{
        title: "犯人姓名",
        field: "prisonerName"
      },
      {
        title: "犯人编号",
        field: "prisonerInfo"
      },
      {
        title: "监区",
        field: "area"
      },
      {
        title: "监室",
        field: "room"
      },
      {
        title: "结算金额",
        field: "settlementMoney",
        dataType: "decimal"
      },
      {
        title: "结算状态",
        field: "status",
        format: toStatus
      },
      {
        title: "结算日期",
        field: "updateTime"
      },
      {
        title: "结算流水号",
        field: "id"
      }
    ],
    showControl: false,
    showCheckbox: false, // 是否显示复选框
    showDetail: false,
    multiSelect: false, // 是否可以多选
    onClickRow: null, // 行单击事件回调
    onDoubleClickRow: null, // 行双击事件回调
  });
});
//判断状态
function toStatus(status) {
  switch (status) {
    case '1':
      return "<span class='label label-success'>待结算</span>";
    case '10':
      return "<span class='label label-default'>已结算</span>";
    default:
      return status;
  }
}

//审核通过
function auditSelectRow() {
  var gridTableList = $$selection.get("gridTable", "prisonerSettlementList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "审核确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认审核选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条数据吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "审核",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("审核通过：通过了" + selectedRows.length + "条数据");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("prisonerSettlement/settlementtoflow", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//审核不通过
function unAuditSelectRow() {
  var gridTableList = $$selection.get("gridTable", "prisonerSettlementList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "审核不通过",
    icon: "fa fa-question-circle-o",
    content: "<p>确认不通过选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条数据吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("审核不通过：不通过" + selectedRows.length + "条数据");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("orderPrisoner/failOrderPrisoner", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}