/**
 * Created by wuxue on 2017/7/13.
 */
$(function () {
  $("#prisonManagementList").gridTable({
    url: "org/pageOrg",
    columns: [{
        title: "监狱监所编号",
        field: "id"
      },
      {
        title: "监狱监所名称",
        field: "orgName"
      },
      {
        title: "联系人",
        field: "contactName"
      },
      {
        title: "联系电话",
        field: "phoneNum"
      },
      {
        title: "地址",
        field: "address"
      },
      {
        title: "监狱监所状态",
        field: "orgStatusStr"
      },
      {
        title: "创建时间",
        field: "createTime"
      }
    ],
    params: {
      orgType: 1
    },
    idField: "id",
    infoUrl: "manage/prisonManagementInfo.html",
    editUrl: "manage/prisonManagementEdit.html",
    onDoubleClickRow: null,
    delUrl: "org/delOrg"
  });
  detail();
});
// 修改文字内容
function detail() {
  $('.showdetail').text('详情')
}