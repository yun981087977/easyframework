$(function () {
  $("#supplierManagementList").gridTable({
    url: "org/pageOrg",
    columns: [{
        title: "供应商编号",
        field: "id"
      },
      {
        title: "供应商名称",
        field: "orgName"
      },
      {
        title: "联系人",
        field: "contactName"
      },
      {
        title: "联系电话",
        field: "phoneNum"
      },
      {
        title: "地址",
        field: "address"
      },
      {
        title: "供应商状态",
        field: "orgStatusStr"
      },
      {
        title: "创建时间",
        field: "createTime"
      }
    ],
    params: {
      orgType: 2
    },
    idField: "id",
    infoUrl: "manage/supplierManagementInfo.html",
    editUrl: "manage/supplierManagementEdit.html",
    delUrl: "org/delOrg",
  });
  detail();
});
// 修改文字内容
function detail() {
  $('.showdetail').text('详情')
}