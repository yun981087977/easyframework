$(function () {
  var MENU_TEMP = "<li><a href='javascript:void(0)'><i class='menu-icon'></i><span class='menu-text'></span><i class='menu-expand'></i></a><ul class='submenu'></ul></li>";

  function Menu(menu) {
    this.element = $(MENU_TEMP);
    this.$name = this.element.children("a").children("span.menu-text");
    this.$icon = this.element.children("a").children("i.menu-icon");
    this.$subMenu = this.element.children("ul.submenu");
    this.$expand = this.element.find("i.menu-expand");
    this.icon = menu.menuIcon;
    this.name = menu.menuName;
    this.link = menu.menuLink;
    this.children = [];
    var that = this;
    this.element.on("click", function () {
      if (that.children.length === 0) {
        if (activatedMenu) {
          activatedMenu.unactive();
        }
        that.active();
        activatedMenu = that;
      } else {
        if (that.element.hasClass("open")) {
          that.close();
        } else {
          that.open();
        }
      }
      return false;
    })
  }
  Menu.prototype.getElement = function () {
    this.$name.text(this.name);
    if (this.icon === undefined || this.icon === null || this.icon.length === 0) {
      this.$icon.remove();
    } else {
      this.$icon.addClass(this.icon);
    }
    return this.element;
  };
  Menu.prototype.addSubMenu = function (menu) {
    var child = new Menu(menu);
    child.removeExpand();
    this.$subMenu.append(child.getElement());
    this.children.push(child);
    return this;
  };
  Menu.prototype.removeExpand = function () {
    this.$expand.remove();
    return this;
  };
  Menu.prototype.open = function () {
    this.element.addClass("open").siblings().removeClass("open");
    return this;
  };
  Menu.prototype.close = function () {
    this.element.removeClass("open");
    return this;
  };
  Menu.prototype.active = function () {
    $("title").text(this.name);
    $("iframe").eq(0).attr("src", this.link);
    this.element.addClass("active");
    return this;
  };
  Menu.prototype.unactive = function () {
    this.element.removeClass("active");
    return this;
  };
  var activatedMenu = null;

  $.doAjax("auth/listTopMenu", function (topMenuList) {
    topMenuList.forEach(function (menu, topDx) {
      var topMenu = new Menu(menu);
      $.doAjax("auth/listSubMenu", {
        parentId: menu.menuId
      }, function (subMenuList) {
        if (subMenuList.length === 0) {
          topMenu.removeExpand();
        } else {
          subMenuList.forEach(function (menu) {
            var menuObj = topMenu.addSubMenu(menu);
          });
        }

        if (topDx === 0) {
          activatedMenu = topMenu.children[0];
          topMenu.open();
          activatedMenu.active();
        }
      });
      $("#menu").append(topMenu.getElement());

    });
  });

  $("#refreshPower").on("click", function () {
    $('#selfIcon').removeClass('glyphicon-triangle-top').addClass('glyphicon-triangle-bottom');
    $.doAjax("auth/refreshPower", function (data) {
      $.confirm({
        title: "刷新权限需要刷新页面",
        icon: "fa fa-question-circle-o",
        content: "<p>是否继续？</p>",
        theme: "modern",
        type: "orange",
        buttons: {
          ok: {
            text: "刷新",
            btnClass: "btn-success",
            action: function () {
              window.location.reload();
            }
          },
          cancel: {
            text: "取消",
            btnClass: "btn-default",
            action: function () {
              // do nothing.
            }
          }
        }
      });
    });
  });
  $('#selfIconRow').on('click', function () {
    if ($('#selfIcon').hasClass('glyphicon-triangle-bottom')) {
      $('#selfIcon').removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-top');
    } else {
      $('#selfIcon').removeClass('glyphicon-triangle-top').addClass('glyphicon-triangle-bottom');
    }
  })

  var nowName = $('#selfIconRow').find('.profile>span').text();
  localStorage.nowName = nowName;
});