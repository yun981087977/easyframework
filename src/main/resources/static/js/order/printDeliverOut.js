// 生成发货单页面
$(function () {
  var params = $.getUrlParams();
  $.doAjax("orderSupplier/getOrderSupplier", params, function (orderSupplier) {
    var status = orderSupplier['status'];
    var statusStr;
    switch (status) {
      case '2':
        statusStr = '待发货'
        break;
      case '5':
        statusStr = '部分发货'
        break;
      default:
        break;
    }
    $("#id").text(orderSupplier['id']);
    $("#totalCount").text(orderSupplier['totalCount']);
    $("#totalSettlement").text('￥' + (Number(orderSupplier['totalSettlement']) / 100).toFixed(2));
    $("#status").text(statusStr);
    $("#updateTime").text(orderSupplier['updateTime']);
    $("#createTime").text(orderSupplier['createTime']);
    $("#area").text(orderSupplier['area']);
    //收货单位详情
    var param = {
      "id": orderSupplier['accountPrisons']
    };
    $.doAjax("org/getOrg", param, function (org) {
      $("#orgName").text(org['orgName']);
      $("#address").text(org['address']);
      $("#phoneNum").text(org['phoneNum']);
    });
  });



});