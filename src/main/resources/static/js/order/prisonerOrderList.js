/**
 * Created by wuxue on 2017/6/30.
 */
$(function () {
  var gridTable = $("#prisonerOrderList").gridTable({
    url: "orderPrisoner/pageOrderPrisoner",
    columns: [{
        title: "订单号",
        field: "id"
      },
      {
        title: "犯人姓名",
        field: "prisonerName"
      },
      {
        title: "监区",
        field: "area"
      },
      {
        title: "状态",
        field: "status",
        format: toStatus
      },
      {
        title: "已发数量",
        field: "totalInvoice",
        dataType: "number"
      },
      {
        title: "商品总数量",
        field: "totalCount",
        dataType: "number"
      },
      {
        title: "订单总额",
        field: "totalMoney",
        dataType: "decimal"
      },
      {
        title: "创建时间",
        field: "createTime"
      }
    ],
    idField: "id",
    infoUrl: "order/prisonerOrderInfo.html",
    controls: {
      edit: {
        show: false
      },
      del: {
        show: false
      }
    },
    onDoubleClickRow: function (row, e) { // 行双击事件回调
      var target = e.target;
      if ($(target).closest("td.row-control").length !== 0) {
        return true;
      }
      var param = {};
      param['id'] = row.getData()['id'];
      $.openIframeDialog({
        url: "order/prisonerOrderInfo.html",
        param: param,
        onOpen: function () {
          row.getElement().addClass("warning");
        },
        onClose: function () {
          row.getElement().removeClass("warning");
          gridTable.reloadData();
        },
        buttons: {
          ok: {
            show: false
          },
          cancel: {
            show: false
          }
        }
      });
    }
  })
});

//判断状态
function toStatus(status) {
  switch (status) {
    case '1':
      return "<span class='label label-default'>待审核</span>";
    default:
      return status;
  }
}

//审核通过
function auditSelectRow() {
  var gridTableList = $$selection.get("gridTable", "prisonerOrderList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "审核确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认审核选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条数据吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "审核",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("审核通过：通过了" + selectedRows.length + "条数据");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("orderPrisoner/passOrderPrisoner", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//审核不通过
function unAuditSelectRow() {
  var gridTableList = $$selection.get("gridTable", "prisonerOrderList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "审核不通过",
    icon: "fa fa-question-circle-o",
    content: "<p>确认不通过选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条数据吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("审核不通过：不通过" + selectedRows.length + "条数据");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("orderPrisoner/failOrderPrisoner", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//生成供应商订单
function addOrderSupplier() {
  $.doAjax("orderSupplier/addOrderSupplier", function () {
    var gridTableList = $$selection.get("gridTable", "prisonerOrderList");
    $.confirm({
      title: "生成供应商订单",
      icon: "fa fa-question-circle-o",
      content: "<p>确认把已审核的订单全部生成供应商订单吗？</p>",
      theme: "modern",
      type: "orange",
      buttons: {
        ok: {
          text: "确定",
          btnClass: "btn-danger",
          action: function () {
            $.success("生成供应商订单");
            gridTableList.reloadData();
          }
        },
        cancel: {
          text: "取消",
          btnClass: "btn-success",
          action: function () {}
        }
      }

    })
  });
}