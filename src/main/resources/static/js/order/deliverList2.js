$(function () {
  $("#deliverList").gridTable({
    url: "invoice/pageInvoice",
    columns: [{
        title: "发货单号",
        field: "id",
        format: Num
      },
      {
        title: "订单号",
        field: "orderSupplierId"
      },
      {
        title: "商品数量",
        field: "totalCount",
        dataType: "number"
      },
      {
        title: "订单金额",
        field: "totalSettlement",
        dataType: "decimal"
      },
      {
        title: "监狱监所",
        field: "prisonsName"
      },
      {
        title: "监区",
        field: "area"
      },
      {
        title: "发货状态",
        field: "statusStr"
      },
      {
        title: "下单日期",
        field: "orderTime"
      },
      {
        title: "商品信息",
        field: "",
        format: showDetails
      },
      {
        title: "操作",
        field: "status",
        format: btnStyle
      }
    ],
    idField: "id",
    params: {
      "status": "1"
    },
    infoUrl: "order/deliverInfo.html",
    showCheckbox: false,
    showDetail: false, // 自定义是否显示查看详情
    showControl: false,
    multiSelect: false, // 是否可以多选
    onClickRow: null, // 行单击事件回调
    onDoubleClickRow: null, // 行双击事件回调
  });
});

function Num(id) {
  return "<span class='Number'>" + id + "</span>";
}

function showDetails() {
  return '<span class="label label-look info Info" id="Info" data-click="lookDetail(this)"><a href="javascript:void(0)">[查看]</a></span>'
}

function btnStyle() {
  return '<span data-menu-id="020301" class="selfBtnControl" data-click="toPutGoods(this)"><a href="javascript:void(0)">[去发货]</a></span>'
}
// 自定义详情
function lookDetail(e) {
  var self_id = $(e).parent().parent().find('.Number').text();
  $.openIframeDialog({
    url: 'order/deliverInfo.html?id=' + self_id,
    title: '商品信息',
    buttons: {
      ok: {
        show: false
      },
      cancel: {
        show: false
      }
    }
  });
}
// 自定义去发货
function toPutGoods(e) {
  var gridTableList = $$selection.get("gridTable", "deliverList");
  localStorage.selfClose = 0; //用于监听页面去发货是否点击确定,然后刷新页面
  var selfClear = setInterval(function () {
    if (localStorage.selfClose == 1) {
      clearInterval(selfClear);
      gridTableList.reloadData();
    } else if (localStorage.selfClose == 2) {
      clearInterval(selfClear);
    }
  }, 100)
  var self_id = $(e).parent().parent().find('.Number').text();
  $.openIframeDialog({
    url: 'order/printDeliverInfoToPutGoods.html?id=' + self_id,
    title: '去发货',
    buttons: {
      ok: {
        show: false
      },
      cancel: {
        show: false
      }
    }
  });
}
//确认发货
function passInvoice() {
  var gridTableList = $$selection.get("gridTable", "deliverList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "发货确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认发出选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条订单吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "发货",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("发货成功：发出了" + selectedRows.length + "条订单");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("invoice/passInvoice", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//确认收货
function confirmInvoice() {
  var gridTableList = $$selection.get("gridTable", "deliverList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "收货确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认发出选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条订单吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "收货",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("收货成功：收到了" + selectedRows.length + "条订单");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("invoice/confirmInvoice", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//生成结算单
function settlement() {
  var gridTableList = $$selection.get("gridTable", "deliverList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "生成结算单确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认生成选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条订单吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "生成结算单",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("生成结算单成功：生成了" + selectedRows.length + "条结算单");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("invoice/settlement", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//缺货登记
function stockOut() {
  var gridTable = $$selection.get("gridTable", "deliverList");
  var selectedRows = gridTable.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("请选择一条数据");
    return false
  }
  if (selectedRows.length > 1) {
    $.error("只允许选择一条数据");
    return false
  }
  var row = selectedRows[0];
  var id = row.getData().id;
  var dialog = $.openIframeDialog({
    title: "缺货登记",
    url: "order/stockOut.html",
    param: {
      idStr: id
    },
    buttons: {
      ok: {
        onclick: function () {
          var form = $("form", dialog.getSubPage())[0];
          var goodsId = $("#goodsId", form).val();
          var goodsCount = $("#goodsCount", form).val();
          var prisonerId = $("#prisonerId", form).val();
          var goodsReturn = {
            goodsId: goodsId,
            goodsCount: goodsCount,
            prisonerId: prisonerId,
            invoiceId: id
          };
          $.doAjax("goodsReturn/addGoodsReturn", goodsReturn, function (data) {
            gridTable.reloadData();
          });
          dialog.close();
        }
      }
    }
  });
}