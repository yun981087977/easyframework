/**
 * Created by wuxue on 2017/6/28.
 */
$(function () {
  var params = $.getUrlParams();
  $.doAjax("invoice/getInvoice", params, function (invoice) {
    $("#id").text(invoice['id']);
    $("#createTime").text(invoice['createTime']);
    $("#area").text(invoice['area']);
    // 收货单位详情
    var param = {
      "id": invoice['accountSupplier']
    };
    $.doAjax("invoice/getOrderSupplierByInvoiceId", params, function (orderSupplier) {
      $("#orderSupplierId").text(orderSupplier['orderSupplierId']);
      $("#xCreateTime").text(orderSupplier['createTime']);
    });
    var param1 = {
      "id": invoice['accountPrisons']
    };
    $.doAjax("org/getOrg", param1, function (org) {
      $("#orgName").text(org['orgName']);
      $("#address").text(org['address']);
      $("#phoneNum").text(org['phoneNum']);
    });
  });
});