$(function () {
  var id = $.getUrlParams()['id'];
  $("#waitDeliverList").gridTable({
    url: "deliver/pageDeliver",
    columns: [{
        title: "监狱监所",
        field: "prisonsName"
      },
      {
        title: "监区",
        field: "area"
      },
      {
        title: "商品图片",
        field: "img",
        format: toImg
      },
      {
        title: "商品名称",
        field: "goodsName"
      },
      {
        title: "商品总数量",
        field: "goodsCount",
        dataType: "number"
      },
      {
        title: "还需发货数量",
        field: "needCount",
        dataType: "number"
      }
    ],
    showControl: false,
    params: {
      id: id
    },
    onDoubleClickRow: null
  });
});

function toImg(url) {
  return "<img src='" + url + "' width='40' height='40'>";
}

//生成结算单
function createOrder() {
  var gridTableList = $$selection.get("gridTable", "waitDeliverList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "生成发货单确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认生成该条发货单吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "生成发货单",
        btnClass: "btn-blue",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            var id = row.getData().id;
            var goodsCount = row.getData().needCount;
            arr.push({
              goodsId: id,
              goodsCount: goodsCount
            })
          });
          var json = JSON.stringify(arr);
          $.doAjax("invoice/addInvoice", {
            idAndCounts: json
          }, function () {
            gridTableList.reloadData();
            $.success("生成发货单成功");
          }, function (error) {
            $.error(error.msg);
          });
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}