$(function () {
  $("#deliverList").gridTable({
    url: "orderSupplier/pageOrderSupplier1",
    columns: [{
        title: "订单号",
        field: "id",
        format: Num
      },
      {
        title: "供应商",
        field: "supplierName"
      },
      {
        title: "监区",
        field: "area"
      },
      {
        title: "订单状态",
        field: "orderSupplierStatus"
      },
      {
        title: "是否缺货",
        field: "stockStatus",
        format: NumStock
      },
      {
        title: "下单日期",
        field: "createTime"
      },
      {
        title: "商品信息",
        field: "",
        format: showDetails
      },
      {
        title: "操作",
        field: "orderSupplierStatus",
        format: btnStyle
      }
    ],
    params: {
      "status": "11"
    },
    idField: "id",
    infoUrl: "order/orderDeliverInfo.html",
    showControl: false,
    showDetail: false,
    onDoubleClickRow: null, // 行双击事件回调
  });
});

function Num(id) {
  return "<span class='Number'>" + id + "</span>";
}

function showDetails() {
  return '<span class="label label-look info Info" id="Info" data-click="lookDetail(this)"><a href="javascript:void(0)">[查看]</a></span>'
}

// 自定义详情
function lookDetail(e) {
  var self_id = $(e).parent().parent().find('.Number').text();
  $.openIframeDialog({
    url: 'order/orderDeliverInfo.html?id=' + self_id,
    title: '商品信息',
    buttons: {
      ok: {
        show: false
      },
      cancel: {
        show: false
      }
    }
  });
}

function btnStyle(orderSupplierStatus) {
  switch (orderSupplierStatus) {
    case '部分发货':
      return '<span data-menu-id="020602" class="selfBtnControl selfDisabled"><a href="javascript:void(0)">[确认收货]</a></span>';
      break;
    default:
      return '<span data-menu-id="020602" data-click="confirm(this);" class="selfBtnControl"><a href="javascript:void(0)">[确认收货]</a></span>';
      break;
  }
}

function NumStock(stockStatus) {
  if (stockStatus == 1) {
    return "缺货";
  } else {
    return "";
  }
}

// 确认收货-单个收货
function confirm(e) {
  var gridTable = $$selection.get("gridTable", "deliverList");
  var selectedRows = gridTable.getSelectedRows();
  var self_id = $(e).parent().parent().find('.Number').text();
  $.confirm({
    title: "收货确认",
    icon: "fa fa-question-circle-o",
    content: "<p>请确认此订单已全部收货，且无缺货信息！</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确认",
        btnClass: "btn-primary",
        action: function () {
          var arr = [];
          arr.push(self_id);
          var idStr = arr.join(",");
          var scb = function () {
            $.success("操作成功!");
            gridTable.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("invoice/confirmInvoice", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  });
}
// 确认收货-批量收货
function confirmInvoice() {
  var gridTableList = $$selection.get("gridTable", "deliverList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "收货确认",
    icon: "fa fa-question-circle-o",
    content: "<p>请确认选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条订单已全部收货，且无缺货信息！</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确认",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("收货成功：收到了" + selectedRows.length + "条订单");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("invoice/confirmInvoice", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}