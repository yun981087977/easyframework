$(function () {
  var params = $.getUrlParams();
  $.doAjax("orderPrisoner/getOrderPrisonerDetail", params, function (order) {
    $("#orderPrisonerDetailId").val(order['orderPrisonerDetailId']);
    $("#goodsImg").attr('src', order['img']);
    $("#goodsName").val(order['goodsName']);
    $("#goodsSellPrice").val(order['goodsSellPrice']);
    $("#goodsSettlementPrice").val(order['goodsSettlementPrice']);
    $("#goodsCount").val(order['goodsCount']);
  });
});