$(function () {
  var id = $.getUrlParams()['id'];
  $("#prisonerOrderInfo").gridTable({
    url: "orderPrisoner/pageOrderPrisonerDetail",
    columns: [{
        title: "商品图片",
        field: "img",
        format: toImg
      },
      {
        title: "商品名称",
        field: "goodsName"
      },
      {
        title: "商品编号",
        field: "goodsId"
      },
      {
        title: "数量",
        field: "goodsCount",
        dataType: "number"
      },
      {
        title: "价格",
        field: "goodsSellPrice",
        dataType: "decimal"
      },
      {
        title: "小计",
        field: "subTotal",
        dataType: "decimal"
      }
    ],
    showControl: false,
    showCheckbox: false,
    showControl: false, // 是否显示操作列
    showDetail: false, // 自定义是否显示查看详情
    params: {
      id: id
    },
    multiSelect: false, // 是否可以多选
    onClickRow: null, // 行单击事件回调
    onDoubleClickRow: null, // 行双击事件回调
  });
});

function toImg(url) {
  return "<img src='" + url + "' width='40' height='40'>";
}