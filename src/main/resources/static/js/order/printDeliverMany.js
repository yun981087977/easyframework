$(function () {
  var params = $.getUrlParams()['idStr'].split('%2C');
  // 循环生成打印的内容
  for (var i = 0; i < params.length; i++) {
    var param = {
      'id': params[i]
    };
    $.ajax({
      type: 'post',
      url: 'invoice/getInvoiceInfo',
      data: param,
      dataType: 'json',
      async: false,
      success: function (data) {
        var releaseSlip = data.data;
        var eachPage = '<div id="printDiv" class="printDiv' + i + ' PageNext"><div class="selfHole"><div class="selfTop"><div class="navTop"><span>收货单信息</span></div><div class="col-xs-6 printNoBottom printNoRight"><div class="form-group"><label class="col-xs-4 control-label"> 收货单号:</label><div class="col-xs-8"><div class="iconic-input right"><span id="id">' + releaseSlip['id'] + '</span></div></div></div></div><div class="col-xs-6 printNoBottom"><div class="form-group"><label class="col-xs-4 control-label"> 订单号:</label><div class="col-xs-8"><div class="iconic-input right"><span id="orderSupplierId">' + releaseSlip['orderSupplierId'] + '</span></div></div></div></div><div class="col-xs-6 printNoRight"><div class="form-group"><label class="col-xs-4 control-label"> 发货时间:</label><div class="col-xs-8"><div class="iconic-input right"><span id="createTime">' + releaseSlip['createTime'] + '</span></div></div></div></div><div class="col-xs-6"><div class="form-group"><label class="col-xs-4 control-label"> 下单时间:</label><div class="col-xs-8"><div class="iconic-input right"><span id="xCreateTime">' + releaseSlip['orderTime'] + '</span></div></div></div></div></div><div class="selfBottom"><div class="navBottom"><span>发货单位信息</span></div><div class="col-xs-6 printNoBottom printNoRight"><div class="form-group"><label class="col-xs-4 control-label"> 单位名称:</label><div class="col-xs-8"><div class="iconic-input right"><span id="orgName">' + releaseSlip['supplierName'] + '</span></div></div></div></div><div class="col-xs-6 printNoBottom"><div class="form-group"><label class="col-xs-4 control-label"> 单位联系人:</label><div class="col-xs-8"><div class="iconic-input right"><span id="contactName">' + releaseSlip['contactName'] + '</span></div></div></div></div><div class="col-xs-6 printNoRight"><div class="form-group"><label class="col-xs-4 control-label"> 地址:</label><div class="col-xs-8"><div class="iconic-input right"><span id="address">' + releaseSlip['address'] + '</span></div></div></div></div><div class="col-xs-6"><div class="form-group"><label class="col-xs-4 control-label"> 联系方式:</label><div class="col-xs-8"><div class="iconic-input right"><span id="phoneNum">' + releaseSlip['phoneNum'] + '</span></div></div></div></div></div></div><div id="deliverInfo' + i + '" class="deliverInfo"></div></div>';
        $('#selfPrint').append(eachPage);
        $("#deliverInfo" + i + "").gridTable({
          url: "invoice/listDetail",
          columns: [{
              title: "商品编号",
              field: "publishDetailId",
            }, {
              title: "商品图片",
              field: "img",
              format: toImg
            },
            {
              title: "商品名称",
              field: "goodsName"
            },
            {
              title: "箱规",
              field: "boxSpec",
            },
            {
              title: "单位",
              field: "goodsUnit",
            },
            {
              title: "单价",
              field: "goodsSellPrice",
              dataType: "decimal"
            },
            {
              title: "数量",
              field: "goodsCount",
              dataType: "number"
            },
            {
              title: "金额",
              field: "cost",
              dataType: "decimal"
            },
            {
              title: "此单发货",
              field: "count",
              dataType: "number"
            },
          ],
          params: param,
          showCheckbox: false,
          idField: "id",
          showDetail: false,
          showControl: false,
          multiSelect: false, // 是否可以多选
          onClickRow: null, // 行单击事件回调
          onDoubleClickRow: null, // 行双击事件回调
        });

        function toImg(url) {
          return "<img src='" + baseUrl + url + "' width='40' height='40'>";
        }
      },
      error: function () {}
    })

  }
});