/*
 *内网个人发放单详情
 */
$(function () {
  var params = {
    id: $.getUrlParams()['id'],
    orderPrisonerId: $.getUrlParams()['orderPrisonerId']
  };
  $.doAjax("releaseSlip/releaseSlipDetail", params, function (releaseSlip) {
    $("#prisonerName").text(releaseSlip['prisonerName']);
    $("#area").text(releaseSlip['area']);
    $("#room").text(releaseSlip['room']);
    $("#id").text(releaseSlip['id']);
    $("#stockOutCount").text(releaseSlip['stockOutCount']);
    $("#totalCount").text(releaseSlip['totalCount']);
    $("#totalMoney").text(((Number(releaseSlip['totalMoney'])) / 100).toFixed(2)); // 单位转换 "分" → "元" 除以100 加"￥"号
  });
});