// 外网: 发货单查询
$(function () {
  $("#deliverList").gridTable({
    url: "invoice/pageInvoice",
    columns: [{
        title: "发货单号",
        field: "id",
        format: Num
      },
      {
        title: "订单号",
        field: "orderSupplierId"
      },
      {
        title: "监狱监所",
        field: "prisonsName"
      },
      {
        title: "监区",
        field: "area"
      },
      {
        title: "发货单状态",
        field: "statusStr"
      },
      {
        title: "发货时间",
        field: "updateTime"
      }
    ],
    idField: "id",
    infoUrl: "order/printDeliverInfoDetailSelf.html", //原版
    showControl: false,
    showCheckbox: false,
    multiSelect: false, // 是否可以多选
    onClickRow: null, // 行单击事件回调
    onDoubleClickRow: null, // 行双击事件回调
  });
  detail();
});

function Num(id) {
  return "<span class='Number'>" + id + "</span>";
}

// 修改文字内容
function detail() {
  $('.showdetail').text('发货单详情')
}

//确认发货
function passInvoice() {
  var gridTableList = $$selection.get("gridTable", "deliverList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "发货确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认发出选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条订单吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "发货",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("发货成功：发出了" + selectedRows.length + "条订单");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("invoice/passInvoice", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//确认收货
function confirmInvoice() {
  var gridTableList = $$selection.get("gridTable", "deliverList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "收货确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认发出选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条订单吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "收货",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("收货成功：收到了" + selectedRows.length + "条订单");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("invoice/confirmInvoice", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//生成结算单
function settlement() {
  var gridTableList = $$selection.get("gridTable", "deliverList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "生成结算单确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认生成选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条订单吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "生成结算单",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("生成结算单成功：生成了" + selectedRows.length + "条结算单");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("invoice/settlement", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//缺货登记
function stockOut() {
  var gridTable = $$selection.get("gridTable", "deliverList");
  var selectedRows = gridTable.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("请选择一条数据");
    return false
  }
  if (selectedRows.length > 1) {
    $.error("只允许选择一条数据");
    return false
  }
  var row = selectedRows[0];
  var id = row.getData().id;
  var dialog = $.openIframeDialog({
    title: "缺货登记",
    url: "order/stockOut.html",
    param: {
      idStr: id
    },
    buttons: {
      ok: {
        onclick: function () {
          var form = $("form", dialog.getSubPage())[0];
          var goodsId = $("#goodsId", form).val();
          var goodsCount = $("#goodsCount", form).val();
          var prisonerId = $("#prisonerId", form).val();
          var goodsReturn = {
            goodsId: goodsId,
            goodsCount: goodsCount,
            prisonerId: prisonerId,
            invoiceId: id
          };
          $.doAjax("goodsReturn/addGoodsReturn", goodsReturn, function (data) {
            gridTable.reloadData();
          });
          dialog.close();
        }
      }
    }
  });
}

//打印收货单
function printOrder(e) {
  var gridTable = $$selection.get("gridTable", "deliverList");
  var selectedRows = gridTable.getSelectedRows();
  var self_id = $(e).parent().parent().find('.Number').text();
  var dialog = $.openIframeDialog({
    title: "打印收货单",
    url: "order/printDeliverInfo.html",
    param: {
      id: self_id
    },
    afterSubmit: function () {
      window.print();
      return true;
    }
  });
}