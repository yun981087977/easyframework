/**
 * Created by jiangyonghao on 2017/1/15.
 * 批量打印个人发放单
 */
$(function () {
  var params = JSON.parse(localStorage.idStrArr);
  // 循环生成打印的内容
  for (var i = 0; i < params.length; i++) {
    var param = {
      'id': params[i].id,
      'orderPrisonerId': params[i].orderPrisonerId
    };
    $.ajax({
      type: 'post',
      url: 'releaseSlip/releaseSlipDetail',
      data: param,
      dataType: 'json',
      async: false,
      success: function (data) {
        var releaseSlip = data.data;
        var eachPage = '<div id="printDiv" class="PageNext innerPrint"><div class="selfHole"><div class="selfTop"><div class="navTop"><span>发放单信息</span></div><div class="col-xs-3 printNoBottom printNoRight"><div class="form-group"><label class="col-xs-5 control-label"> 姓名：</label><div class="col-xs-7"><div class="iconic-input right"><span id="prisonerName">' + releaseSlip["prisonerName"] + '</span></div></div></div></div><div class="col-xs-4 printNoBottom printNoRight"><div class="form-group"><label class="col-xs-4 control-label"> 监室：</label><div class="col-xs-8"><div class="iconic-input right"><span id="area">' + releaseSlip["area"] + '</span><span></span><span id="room">' + releaseSlip["room"] + '</span></div></div></div></div><div class="col-xs-5 printNoBottom"><div class="form-group"><label class="col-xs-4 control-label"> 发放单号：</label><div class="col-xs-8"><div class="iconic-input right"><span id="id">' + releaseSlip["id"] + '</span></div></div></div></div><div class="col-xs-12"><div class="form-group"><label class="col-xs-2 control-label"> 缺货处理：</label><div class="col-xs-10"><div class="iconic-input right"><span>遵循“有货先发”原则，将有货的商品进行优先配送。发放商品时按订单下单时间先后顺序依次发放，先下单者优先发放，未收货者待后续补货到货时再行发放。</span></div></div></div></div></div></div><div id="deliverInfo' + i + '" class="deliverInfo"></div><div class="hole" align="right"><p class="top">= 应发放总数：<span id="totalCount">' + releaseSlip["totalCount"] + '</span></p><p class="bottom">= 订单总金额：￥<span id="totalMoney">' + ((Number(releaseSlip["totalMoney"])) / 100).toFixed(2) + '</span>元</p></div><div class="selfBottom"><div class="navBottom"><span>确认收货</span></div><div class="col-xs-6"><div class="form-group"><label class="col-xs-6 control-label"> 签名（盖指印）：</label><div class="col-xs-6"><div class="iconic-input right"><span id="orgName"></span></div></div></div></div><div class="col-xs-6"><div class="form-group"><label class="col-xs-4 control-label"> 收货日期：</label><div class="col-xs-8"><div class="iconic-input right"><span id="contactName"></span></div></div></div></div></div></div>';
        $('#selfPrint').append(eachPage);
        $("#deliverInfo" + i + "").gridTable({
          url: "releaseSlip/listReleaseSlipDetail",
          columns: [{
              title: "商品编号",
              field: "goodsId"
            }, {
              title: "商品图片",
              field: "img",
              format: toImg
            },
            {
              title: "商品名称",
              field: "goodsName"
            },
            {
              title: "应发放数量",
              field: "goodsCount"
            },
            {
              title: "价格",
              field: "goodsSellPrice",
              dataType: "decimal"
            },
            {
              title: "小计",
              field: "subTotal",
              dataType: "decimal"
            },
            {
              title: "缺货数量",
              field: ""
            }
          ],
          params: param,
          showCheckbox: false,
          idField: "id",
          showDetail: false,
          showControl: false,
          multiSelect: false, // 是否可以多选
          onClickRow: null, // 行单击事件回调
          onDoubleClickRow: null, // 行双击事件回调
        });

        function toImg(url) {
          return "<img src='" + baseUrl + url + "' width='40' height='40'>";
        }
      },
      error: function () {}
    })

  }
});