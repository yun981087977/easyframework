/**
 * Created by wuxue on 2017/6/28.
 */
$(function () {
  var params = $.getUrlParams();
  $.doAjax("invoice/getInvoiceInfo", params, function (invoice) {
    $("#id").text(invoice['id']);
    $("#createTime").text(invoice['createTime']);
    $("#orderSupplierId").text(invoice['orderSupplierId']);
    $("#xCreateTime").text(invoice['orderTime']);
    $("#orgName").text(invoice['supplierName']);
    $("#contactName").text(invoice['contactName']);
    $("#address").text(invoice['address']);
    $("#phoneNum").text(invoice['phoneNum']);
  });
});