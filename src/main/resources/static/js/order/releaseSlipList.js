/**
 * Created by wuxue on 2017/8/1. 个人发放单页面
 */
$(function () {
  $("#releaseSlipList").gridTable({
    url: "releaseSlip/pageReleaseSlip",
    columns: [{
        title: "订单号",
        field: "id",
        format: Num
      },
      {
        title: "监室",
        field: "room"
      },
      {
        title: "姓名",
        field: "prisonerName"
      },
      {
        title: "应发放数量",
        field: "totalCount",
        dataType: "number"
      },
      {
        title: "创建时间",
        field: "createTime"
      },
      {
        title: "发放单详情",
        field: "orderPrisonerId",
        format: showDetails
      },
      {
        title: "操作",
        field: "orderPrisonerId",
        format: btnStyle
      }
    ],
    idField: "id",
    showDetail: false,
    showControl: false, // 是否显示操作列
    infoUrl: "order/printDeliverOutSelf.html",
    onDoubleClickRow: null, // 行双击事件回调
  });
  detail();
});

function Hide(orderPrisonerId) {
  return "<span class='orderPrisonerId'>" + orderPrisonerId + "</span>";
}

function showDetails(orderPrisonerId) {
  return '<span class="label label-look info Info" data-orderPrisonerId=' + orderPrisonerId + ' id="Info" data-click="lookDetail(this)"><a href="javascript:void(0)">[查看]</a></span>'
}
// 自定义详情
function lookDetail(e) {
  var id = $(e).parent().parent().find('.Number').text();
  var self_id = $(e).attr('data-orderPrisonerId');
  $.openIframeDialog({
    url: 'order/printDeliverOutSelfDetail.html?orderPrisonerId=' + self_id + '&id=' + id,
    title: '发放单详情',
    buttons: {
      ok: {
        show: false
      },
      cancel: {
        show: false
      }
    }
  });
}

// 自定义操作栏内容
function btnStyle(orderPrisonerId) {
  return '<span data-menu-id="040120" data-click="makeList(this)" data-orderPrisonerId=' + orderPrisonerId + ' class="selfBtnControl">[打印发放单]</span>&nbsp;&nbsp;&nbsp;<span data-menu-id="040110" data-orderPrisonerId=' + orderPrisonerId + ' data-click="lessList(this)" class="selfBtnControl">[缺货登记]</span>'
}

function Num(id) {
  return "<span class='Number'>" + id + "</span>";
}

// 打印发放单
function makeList(e) {
  var id = $(e).parent().parent().find('.Number').text();
  var self_id = $(e).attr('data-orderPrisonerId');
  $.openIframeDialog({
    title: "打印发放单",
    param: {
      id: id,
      orderPrisonerId: self_id
    },
    url: "order/printDeliverOutSelf.html",
    buttons: {
      ok: {
        show: false
      },
      cancel: {
        show: false
      },
    }
  });
}
//批量打印个人发放单
function printOrderMany() {
  var gridTableList = $$selection.get("gridTable", "releaseSlipList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  var arr = [];
  selectedRows.forEach(function (row) {
    var paramsObj = {};
    paramsObj.id = row.getData().id;
    paramsObj.orderPrisonerId = row.getData().orderPrisonerId;
    arr.push(paramsObj);
    row.setStyleEdit();
  });
  var idStr = JSON.stringify(arr);
  localStorage.idStrArr = idStr;
  var dialog = $.openIframeDialog({
    title: "批量打印个人发放单",
    url: "order/printDeliverManyInner.html",
    buttons: {
      ok: {
        show: false
      },
      cancel: {
        show: false
      },
    }
  });
}
// 修改文字内容
function detail() {
  $('.showdetail').text('发放单详情')
}
// 缺货登记
function lessList(e) {
  var gridTableList = $$selection.get("gridTable", "releaseSlipList");
  localStorage.selfClose = 0; //用于监听页面缺货登记是否点击确定,然后刷新页面
  var selfClear = setInterval(function () {
    if (localStorage.selfClose == 1) {
      clearInterval(selfClear);
      gridTableList.reloadData();
    } else if (localStorage.selfClose == 2) {
      clearInterval(selfClear);
    }
  }, 100)
  var self_id = $(e).attr('data-orderPrisonerId');
  var id = $(e).parent().parent().find('.Number').text();
  $.openIframeDialog({
    title: "缺货登记",
    url: "order/releaseGoodsInfoSelf.html?orderPrisonerId=" + self_id + "&id=" + id,
    buttons: {
      ok: {
        show: false
      },
      cancel: {
        show: false
      }
    }
  });
}