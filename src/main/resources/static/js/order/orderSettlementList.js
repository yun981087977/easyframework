$(function () {
  $("#deliverList").gridTable({
    url: "orderSupplier/pageOrderSupplierNotSettle",
    columns: [{
        title: "订单号",
        field: "id",
        format: Num
      },
      {
        title: "结算金额",
        field: "totalSettlement",
        dataType: "decimal"
      },
      {
        title: "供应商",
        field: "supplierName"
      },
      {
        title: "订单状态",
        field: "orderSupplierStatus"
      },
      {
        title: "结算状态",
        field: "settlementId",
        format: toStatus
      },
      {
        title: "收货时间",
        field: "updateTime"
      },
      {
        title: "订单详情",
        field: "id",
        format: showDetails
      },
      {
        title: "操作",
        field: "id",
        format: btnStyle
      }
    ],
    params: {
      "status": "6"
    },
    idField: "id",
    infoUrl: "order/supplierInfo.html",
    showDetail: false, // 自定义是否显示查看详情
    showControl: false,
    onDoubleClickRow: null, // 行双击事件回调
  });
});

function Num(id) {
  return "<span class='Number'>" + id + "</span>";
}

function toStatus(settlementId) {
  return "<span class='label label-success'>未结算</span>";
}

function showDetails() {
  return '<span class="label label-look info Info" id="Info" data-click="lookDetail(this)"><a href="javascript:void(0)">[查看]</a></span>'
}

// 自定义详情
function lookDetail(e) {
  var self_id = $(e).parent().parent().find('.Number').text();
  $.openIframeDialog({
    url: 'order/orderDeliverInfo.html?id=' + self_id,
    buttons: {
      ok: {
        show: false
      },
      cancel: {
        show: false
      }
    }
  });
}

// 自定义操作栏内容
function btnStyle() {
  return '<span data-menu-id="020701" data-click="settlementSelf(this);" class="selfBtnControl">[生成结算单]</span>';
}

//生成结算单---批量生成结算单
function settlement() {
  var gridTableList = $$selection.get("gridTable", "deliverList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "生成结算单确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认生成选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条订单吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "生成结算单",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("生成结算单成功：生成了" + selectedRows.length + "条结算单");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("invoice/settlement", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//生成结算单---单个生成结算单
function settlementSelf(e) {
  var gridTableList = $$selection.get("gridTable", "deliverList");
  var selectedRows = gridTableList.getSelectedRows();
  var self_id = $(e).parent().parent().find('.Number').text();
  $.confirm({
    title: "生成结算单确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确定生成该商品的结算单吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          arr.push(self_id);
          var idStr = arr.join(",");
          var scb = function () {
            $.success("生成结算单成功!");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("invoice/settlement", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}