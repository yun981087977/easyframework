/**
 * Created by zhangt on 2017/7/21.
 */
$(document).ready(function () {
  $('#defaultForm').bootstrapValidator({
    message: '这个值是无效的',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      username: {
        message: '用户名验证失败',
        validators: {
          notEmpty: {
            message: '用户名不能为空'
          },
          stringLength: {
            min: 6,
            max: 18,
            message: '用户名长度必须在6到18位之间'
          }
          // regexp: {
          //     regexp: /^[a-zA-Z0-9_\.]+$/,
          //     message: '用户名只能包含大写、小写、数字和下划线'
          // }
        }
      },
      country: {
        validators: {
          notEmpty: {
            message: '表单不能为空'
          }
        }
      },
      acceptTerms: {
        validators: {
          notEmpty: {
            message: '你必须接受条款和政策'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: '表单不能为空'
          },
          emailAddress: {
            message: '输入不是一个有效的电子邮件地址'
          }
        }
      },
      website: {
        validators: {
          uri: {
            allowLocal: true,
            message: '不是有效网站'
          }
        }
      },
      color: {
        validators: {
          color: {
            type: ['hex', 'rgb', 'hsl', 'keyword'],
            message: 'Must be a valid %s color'
          }
        }
      },
      colorAll: {
        validators: {
          color: {}
        }
      },
      zipCode: {
        validators: {
          zipCode: {
            country: 'US',
            message: '不是有效的邮政编码'
          }
        }
      },
      password: {
        validators: {
          notEmpty: {
            message: '密码不能为空'
          },
          identical: {
            field: 'confirmPassword',
            message: '密码不正确'
          }
        }
      },
      confirmPassword: {
        validators: {
          notEmpty: {
            message: '再次确认密码不能为空'
          },
          identical: {
            field: 'password',
            message: '再次确认密码不正确'
          }
        }
      },
      ages: {
        validators: {
          lessThan: {
            value: 100,
            inclusive: true,
            message: 'The ages has to be less than 100'
          },
          greaterThan: {
            value: 10,
            inclusive: false,
            message: 'The ages has to be greater than or equals to 10'
          }
        }
      }
    }
  });
});