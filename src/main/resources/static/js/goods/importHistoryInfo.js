$(function () {
  var id = $.getUrlParams()['id'];
  $("#historyInfo").gridTable({
    url: "goodsHistory/listGoodsHistory",
    columns: [{
        title: "商品图片",
        field: "goodsImg",
        format: toImg
      },
      {
        title: "商品名称",
        field: "goodsName"
      },
      {
        title: "零售价",
        field: "goodsMarketPrice",
        dataType: "decimal"
      },
      {
        title: "供应价",
        field: "goodsSellPrice",
        dataType: "decimal"
      },
      {
        title: "结算价",
        field: "goodsSettlementPrice",
        dataType: "decimal"
      },
      {
        title: "单位",
        field: "goodsUnit"
      },
      {
        title: "商品状态",
        field: "goodsStatusStr"
      }
    ],
    onDoubleClickRow: null,
    showControl: false,
    showCheckbox: false,
    params: {
      id: id
    }
  });
});

function toImg(url) {
  return "<img src='" + url + "' width='80' height='80'>";
}