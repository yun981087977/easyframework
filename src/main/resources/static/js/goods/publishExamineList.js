/**
 * Created by wuxue on 2017/7/20.
 */
$(function () {
  $("#publishList").gridTable({
    url: "goodsBakPublish/pagePublishDetailPlus",
    columns: [{
        title: "商品图片",
        field: "goodsImg",
        format: toImg
      },
      {
        title: "上架商品编号",
        field: "id",
        format: Num
      },
      {
        title: "监狱监所",
        field: "prisonsName"
      },
      {
        title: "供应商",
        field: "orgName"
      },
      {
        title: "商品类型",
        field: "goodsTypeName"
      },
      {
        title: "商品名称",
        field: "goodsName"
      },
      {
        title: "零售价",
        field: "goodsMarketPrice",
        dataType: "decimal"
      },
      {
        title: "供应价",
        field: "goodsSellPrice",
        dataType: "decimal"
      },
      {
        title: "结算价",
        field: "goodsSettlementPrice",
        dataType: "decimal"
      },
      {
        title: "商品状态",
        field: "goodsStatus",
        format: toStatus
      },
      {
        title: "单位",
        field: "goodsUnit"
      },
      {
        title: "规格",
        field: "goodsSpec"
      },
      {
        title: "箱规",
        field: "boxSpec"
      },
      {
        title: "条码",
        field: "goodsBarCode"
      },
    ],
    params: {
      "goodsStatus": "10",
      "prisonsId": $("#prisonsId").val()
    },
    idField: "id",
    infoUrl: "goods/publishInfo.html",
    showControl: false,
    showDetail: false, // 自定义是否显示查看详情
    selfControlNewgoods: true, //自定义操作列, 内容: 编辑/下架申请
    showCheckbox: false,
    editUrl: "goods/outerOnlineGoodsEdit.html",
    multiSelect: false, // 是否可以多选
    onClickRow: null, // 行单击事件回调
    onDoubleClickRow: null, // 行双击事件回调
  });
  var changeClick = setInterval(weatherClick, 100); //更改按钮状态
});
var changeClick = setInterval(weatherClick, 100); //更改按钮状态

function Num(ID) {
  return "<span class='Number'>" + ID + "</span>";
}

function publishGoods() {
  window.location.href = "goods/publishGoodsInfo.html";
}

//发布商品
function printOrder() {
  var dialog = $.openIframeDialog({
    title: "发布商品",
    url: "goods/publishGoodsInfo.html",
    param: {},
    buttons: {
      ok: {
        onclick: function () {}
      }
    },
    afterSubmit: function () {
      return true;
    },
  });
}
// 上架申请
function upAudit() {
  var gridTable = $$selection.get("gridTable", "publishList");
  var selectedRows = gridTable.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "上架申请确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认将选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条数据进行上架申请吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "申请",
        btnClass: "btn-primary",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("申请成功：申请了" + selectedRows.length + "条数据");
            gridTable.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("goodsPublish/upAudit", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  });
}
//上架通过
function upAuditPass() {
  var gridTable = $$selection.get("gridTable", "publishList");
  var selectedRows = gridTable.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "上架通过确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认让选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条商品上架吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "上架",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("审核通过：通过了" + selectedRows.length + "条数据");
            gridTable.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("goodsPublish/upAuditPass", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  });
}
//上架不通过
function upAuditFail() {
  var gridTable = $$selection.get("gridTable", "publishList");
  var selectedRows = gridTable.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "审核不通过",
    icon: "fa fa-question-circle-o",
    content: "<p>确认不通过选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条数据吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("审核不通过：不通过" + selectedRows.length + "条数据");
            gridTable.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("goodsPublish/upAuditFail", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//下架商品
function downAudit(e) {
  var gridTableList = $$selection.get("gridTable", "publishList");
  var selectedRows = gridTableList.getSelectedRows();
  var self_id = $(e).parent().parent().find('.Number').text();
  $.confirm({
    title: "下架确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确定下架该商品吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          arr.push(self_id);
          var idStr = arr.join(",");
          var scb = function () {
            $.success("商品下架申请已提交!");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("goodsPublish/downAudit", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//下架通过
function downAuditPass() {
  var gridTableList = $$selection.get("gridTable", "publishList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "下架审核通过",
    icon: "fa fa-question-circle-o",
    content: "<p>确认审核选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条数据吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "下架",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("审核通过：通过了" + selectedRows.length + "条数据");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("goodsPublish/downAuditPass", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//下架不通过
function downAuditFail() {
  var gridTableList = $$selection.get("gridTable", "publishList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "审核不通过",
    icon: "fa fa-question-circle-o",
    content: "<p>确认不通过选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条数据吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("审核不通过：不通过" + selectedRows.length + "条数据");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("goodsPublish/downAuditFail", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//下架商品
function down() {
  var gridTableList = $$selection.get("gridTable", "publishList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "下架确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认下架选中的 <span class='text-danger'>" + selectedRows.length + "</span> 件商品吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "下架",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("下架了" + selectedRows.length + "件商品");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("goodsPublish/down", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
// 驳回处理
function failConfirm() {
  var gridTableList = $$selection.get("gridTable", "publishList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "驳回处理",
    icon: "fa fa-question-circle-o",
    content: "<p>驳回处理会将『下架驳回』的商品重新变更为『上架』<br>确认处理选中的 <span class='text-danger'>" + selectedRows.length + "</span> 件商品吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "处理",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("处理了" + selectedRows.length + "件商品");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("goodsPublish/failConfirm", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//批量删除
function batchDel() {
  var gridTableList = $$selection.get("gridTable", "publishList");
  var selectedRows = gridTableList.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "删除确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认删除选中的 <span class='text-danger'>" + selectedRows.length + "</span> 件商品吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "删除",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("删除了" + selectedRows.length + "件商品");
            gridTableList.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("goodsPublish/batchDel", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}

function toImg(url) {
  return "<img src='" + url + "' width='40' height='40'>";
}

function toStatus(goodsStatus) {
  switch (goodsStatus) {
    case '40':
      return "<span class='label label-danger selfStatus'>下架驳回</span>";
    case '50':
      return "<span class='label label-danger selfStatus'>编辑驳回</span>";
    case '60':
      return "<span class='label label-yellow selfStatus' data-status='9'>编辑待审核</span>";
    case '70':
      return "<span class='label label-yellow selfStatus' data-status='9'>下架待审核</span>";
    default:
      return "<span class='label label-success' data-status=" + goodsStatus + "></span>";
  }
}

function weatherClick() {
  $('.row-newgoods').each(function () {
    if ($(this).parent().children().find('.selfStatus').attr('data-status') == '9' && $(this).find('.selfBtnControl').attr('data-click') == '') {
      clearInterval(changeClick);
    } else if ($(this).parent().children().find('.selfStatus').attr('data-status') == '9') {
      $(this).find('.edit').attr('data-selfOpen', 'no'); //禁用编辑
      $(this).find('.selfBtnControl').attr('data-click', ''); //禁用申请下架
      $(this).find('.edit').find('a').css('color', '#666'); //更改禁用样式
      $(this).find('.selfBtnControl').css('color', '#666'); //更改禁用样式
    }
  })
}
setTimeout(function () {
  clearInterval(changeClick);
}, 10000);