/**
 * Created by wuxue on 2017/6/28.
 */
$(function () {
  var params = $.getUrlParams();
  $.doAjax("goods/getGoods", params, function (goods) {
    if (goods['boxSpec'] === null) {
      $("#boxSpec").text();
    } else {
      $("#boxSpec").text(goods['boxSpec']);
    }
    if (goods['goodsSpec'] === null) {
      $("#goodsSpec").text();
    } else {
      $("#goodsSpec").text(goods['goodsSpec']);
    }
    if (goods['goodsBarCode'] === null) {
      $("#goodsBarCode").text();
    } else {
      $("#goodsBarCode").text(goods['goodsBarCode']);
    }
    $("#goodsImg").attr("src", "" + goods.goodsImg + "");
    $("#goodsName").text(goods['goodsName']);
    $("#goodsMarketPrice").text("￥" + (Number(goods['goodsMarketPrice'])) / 100);
    $("#goodsSellPrice").text("￥" + (Number(goods['goodsSellPrice'])) / 100);
    $("#goodsSettlementPrice").text("￥" + (Number(goods['goodsSettlementPrice'])) / 100);
    $("#goodsUnit").text(goods['goodsUnit']);
  });

});