/**
 * Created by wuxue on 2017/7/4.
 */
$(function () {
  $("#importHistoryList").gridTable({
    url: "goodsHistory/pageGoodsHistory",
    columns: [{
        title: "编号",
        field: "id"
      },
      {
        title: "供应商",
        field: "orgName"
      },
      {
        title: "导入时间",
        field: "createTime"
      }
    ],
    showCheckbox: false,
    idField: "id",
    infoUrl: "goods/importHistoryInfo.html",
    controls: {
      edit: {
        show: false
      },
      del: {
        show: false
      }
    }
  });
});