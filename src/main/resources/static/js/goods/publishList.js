$(function () {
  $("#publishList").gridTable({
    url: "goodsLog/pageGoodsLog",
    columns: [{
        title: "商品编号",
        field: "goodsId"
      },
      {
        title: "商品图片",
        field: "goodsImg",
        format: toImg
      },
      {
        title: "商品名称",
        field: "goodsName"
      },
      {
        title: "商品类型",
        field: "goodsTypeName"
      },
      {
        title: "监狱监所",
        field: "prisonsName"
      },
      {
        title: "上架状态",
        field: "goodsStatus",
        format: toStatus
      },
      {
        title: "审核类别",
        field: "examineStatus",
        format: toExamineStatus
      },
      {
        title: "驳回原因",
        field: "reason"
      },
      {
        title: "驳回时间",
        field: "updateTime"
      }
    ],
    params: {
      'goodsStatus': '5'
    },
    idField: "id",
    infoUrl: "goods/publishLogInfo.html",
    showControl: false,
    multiSelect: false, // 是否可以多选
    showCheckbox: false, // 是否显示复选框
    onClickRow: null, // 行单击事件回调
    onDoubleClickRow: null, // 行双击事件回调
  });
  detail();
});
// 修改文字内容
function detail() {
  $('.showdetail').text('商品详情')
}

function toExamineStatus(examineStatus) {
  switch (examineStatus) {
    case '1':
      return "<span class='label label-success'>新增</span>";
    case '2':
      return "<span class='label label-look'>编辑</span>";
    case '3':
      return "<span class='label label-danger'>下架</span>";
    default:
      return examineStatus;
  }
}

function toImg(url) {
  return "<img src='" + url + "' width='40' height='40'>";
}

function toStatus(goodsStatus) {
  switch (goodsStatus) {
    case '5':
      return "<span class='label label-danger'>审核驳回</span>";
    case '9':
      return "<span class='label label-danger'>待审核</span>";
    case '10':
      return "<span class='label label-success'>审核通过</span>";
    default:
      return goodsStatus;
  }
}

/**
 * 提交筛选--自定义(切换审核状态刷新页面显示对应的数据)
 */
localStorage.statusSelf = 5; //用于判断点击查询时是否需清除已驳回/待审核/审核通过 这三个按钮的样式,默认已驳回
function submitFilterSelf(gridId, status, e) {
  var status = status || $('#goodsStatus').val();
  if (status == 5) {
    $("#publishList").empty().gridTable({
      url: "goodsLog/pageGoodsLog",
      columns: [{
          title: "商品编号",
          field: "goodsId"
        },
        {
          title: "商品图片",
          field: "goodsImg",
          format: toImg
        },
        {
          title: "商品名称",
          field: "goodsName"
        },
        {
          title: "商品类型",
          field: "goodsTypeName"
        },
        {
          title: "监狱监所",
          field: "prisonsName"
        },
        {
          title: "上架状态",
          field: "goodsStatus",
          format: toStatus
        },
        {
          title: "审核类别",
          field: "examineStatus",
          format: toExamineStatus
        },
        {
          title: "驳回原因",
          field: "reason"
        },
        {
          title: "驳回时间",
          field: "updateTime"
        }
      ],
      params: {
        'goodsStatus': '5'
      },
      idField: "id",
      infoUrl: "goods/publishLogInfo.html",
      showControl: false,
      multiSelect: false, // 是否可以多选
      showCheckbox: false, // 是否显示复选框
      onClickRow: null, // 行单击事件回调
      onDoubleClickRow: null, // 行双击事件回调
    });
    detail();
  } else if (status == 9) {
    $("#publishList").empty().gridTable({
      url: "goodsLog/pageGoodsLog",
      columns: [{
          title: "商品编号",
          field: "goodsId"
        },
        {
          title: "商品图片",
          field: "goodsImg",
          format: toImg
        },
        {
          title: "商品名称",
          field: "goodsName"
        },
        {
          title: "商品类型",
          field: "goodsTypeName"
        },
        {
          title: "监狱监所",
          field: "prisonsName"
        },
        {
          title: "上架状态",
          field: "goodsStatus",
          format: toStatus
        },
        {
          title: "审核类别",
          field: "examineStatus",
          format: toExamineStatus
        },
        {
          title: "提交时间",
          field: "createTime"
        }
      ],
      params: {
        'goodsStatus': '9'
      },
      idField: "id",
      infoUrl: "goods/publishLogInfo.html",
      showControl: false,
      multiSelect: false, // 是否可以多选
      showCheckbox: false, // 是否显示复选框
      onClickRow: null, // 行单击事件回调
      onDoubleClickRow: null, // 行双击事件回调
    });
    detail();
  } else if (status == 10) {
    $("#publishList").empty().gridTable({
      url: "goodsLog/pageGoodsLog",
      columns: [{
          title: "商品编号",
          field: "goodsId"
        },
        {
          title: "商品图片",
          field: "goodsImg",
          format: toImg
        },
        {
          title: "商品名称",
          field: "goodsName"
        },
        {
          title: "商品类型",
          field: "goodsTypeName"
        },
        {
          title: "监狱监所",
          field: "prisonsName"
        },
        {
          title: "上架状态",
          field: "goodsStatus",
          format: toStatus
        },
        {
          title: "审核类别",
          field: "examineStatus",
          format: toExamineStatus
        },
        {
          title: "审核通过时间",
          field: "updateTime"
        }
      ],
      params: {
        'goodsStatus': '10'
      },
      idField: "id",
      infoUrl: "goods/publishLogInfo.html",
      showControl: false,
      multiSelect: false, // 是否可以多选
      showCheckbox: false, // 是否显示复选框
      onClickRow: null, // 行单击事件回调
      onDoubleClickRow: null, // 行双击事件回调
    });
    detail();
  }
  $('#goodsStatus').val(status);
  if (localStorage.statusSelf != status) { //判断是否需清除按钮样式
    $('.selfHeader').find('.statu').removeClass('hasChecked');
    $(e).addClass('hasChecked');
  }
  var gridTable = $$selection.get("gridTable", gridId);
  var $filter = $(".grid-table .filter");
  var json = $filter.serializeJson();
  json.goodsStatus = status;
  localStorage.statusSelf = status;
  $.extend(gridTable.getRequestParam(), json);
  gridTable.reloadData();
  $filter.removeClass("show");
}