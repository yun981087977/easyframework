$(function () {
  $("#goodsList").gridTable({
    url: "goodsBakPublish/pagePublishDetailPlus",
    columns: [{
        title: "商品编号",
        field: "id"
      },
      {
        title: "商品图片",
        field: "goodsImg",
        format: toImg
      },
      {
        title: "商品名称",
        field: "goodsName"
      },
      {
        title: "规格",
        field: "goodsSpec"
      },
      {
        title: "零售价",
        field: "goodsMarketPrice",
        dataType: "decimal"
      },
      {
        title: "供应价",
        field: "goodsSellPrice",
        dataType: "decimal"
      },
      {
        title: "结算价",
        field: "goodsSettlementPrice",
        dataType: "decimal"
      },
      {
        title: "单位",
        field: "goodsUnit"
      },
      {
        title: "商品类型",
        field: "goodsTypeName"
      },
      {
        title: "供应商",
        field: "orgName"
      },
      {
        title: "上架时间",
        field: "updateTime"
      }
    ],
    params: {
      "goodsStatus": "10"
    },
    pageSize: 10,
    idField: "id",
    infoUrl: "goods/outerGoodsPublishInfo.html",
    editUrl: "goods/outerGoodsEdit.html",
    delUrl: "goods/delGoods",
    showControl: false,
    showDetail: false, // 自定义是否显示查看详情
    showCheckbox: false, // 是否显示复选框
    multiSelect: false, // 是否可以多选
    onClickRow: null, // 行单击事件回调
    onDoubleClickRow: null, // 行双击事件回调
  });
});

function toStatus(goodsStatus) {
  switch (goodsStatus) {
    case '5':
      return "<span class='label label-danger'>审核驳回</span>";
    case '9':
      return "<span class='label label-danger'>待审核</span>";
    case '10':
      return "<span class='label label-success'>审核通过</span>";
    default:
      return goodsStatus;
  }
}

function toExamineStatus(examineStatus) {
  switch (examineStatus) {
    case '1':
      return "<span class='label label-success'>新增</span>";
    case '2':
      return "<span class='label label-look'>编辑</span>";
    case '3':
      return "<span class='label label-danger'>下架</span>";
    default:
      return examineStatus;
  }
}

function toImg(url) {
  return "<img src='" + url + "' width='40' height='40'>";
}

//上架商品
function add1() {
  var gridTable = $$selection.get("gridTable", "goodsList");
  var selectedRows = gridTable.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("请选择一个商品");
    return false;
  }
  if (selectedRows.length > 1) {
    $.error("只允许选择一条数据");
    return false
  }
  var row = selectedRows[0];
  var id = row.getData().id;
  var dialog = $.openIframeDialog({
    title: "新增上架商品",
    url: "goods/upGoods.html",
    param: {
      id: id
    },
    afterSubmit: function () {
      gridTable.reloadData();
      return true;
    }
  });
}
//上架通过
function upAuditPass() {
  var gridTable = $$selection.get("gridTable", "goodsList");
  var selectedRows = gridTable.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "上架通过确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认让选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条商品上架吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "上架",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("审核通过：通过了" + selectedRows.length + "条数据");
            gridTable.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("goodsPublish/upAuditPass", {
            "idStr": idStr
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  });
}
//上架不通过
function upAuditFail() {
  var gridTable = $$selection.get("gridTable", "goodsList");
  var selectedRows = gridTable.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  $.confirm({
    title: "审核不通过",
    icon: "fa fa-question-circle-o",
    content: "驳回原因:<input type='text' name='reason' id='reason' />",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-danger",
        action: function () {
          var arr = [];
          selectedRows.forEach(function (row) {
            arr.push(row.getData().id);
          });
          var idStr = arr.join(",");
          var scb = function () {
            $.success("审核不通过：不通过" + selectedRows.length + "条数据");
            gridTable.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          var reason = $("#reason").val();
          $.doAjax("goodsPublish/upAuditFail", {
            "idStr": idStr,
            "reason": reason
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}