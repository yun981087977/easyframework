/**
 * Created by wuxue on 2017/7/26.
 */
$(function () {
  $("#goodsList").gridTable({
    url: "innerGoods/pageGoods",
    columns: [{
        title: "商品编号",
        field: "id",
        format: Num
      },
      {
        title: "商品图片",
        field: "goodsImg",
        format: toImg
      },
      {
        title: "商品名称",
        field: "goodsName"
      },
      {
        title: "规格",
        field: "goodsSpec"
      },
      {
        title: "零售价",
        field: "goodsMarketPrice",
        dataType: "decimal"
      },
      {
        title: "供应价",
        field: "goodsSellPrice",
        dataType: "decimal"
      },
      {
        title: "结算价",
        field: "goodsSettlementPrice",
        dataType: "decimal"
      },
      {
        title: "单位",
        field: "goodsUnit"
      },
      {
        title: "商品类型",
        field: "goodsTypeName"
      },
      {
        title: "供应商",
        field: "orgName"
      },
      {
        title: "创建时间",
        field: "createTime"
      },
      {
        title: "操作",
        field: "goodsStatus",
        format: btnStyle
      }
    ],
    pageSize: 10,
    idField: "id",
    showCheckbox: false, // 是否显示复选框
    showDetail: false, // 自定义是否显示查看详情
    showControl: false, // 是否显示操作列
    multiSelect: false, // 是否可以多选
    onClickRow: null, // 行单击事件回调
    onDoubleClickRow: null, // 行双击事件回调
  });
});

// 商品操作栏内容
function btnStyle(goodsStatus) {
  if (goodsStatus == '90') {
    return '<span data-menu-id="010101" data-click="goodsPublish(this);" class="selfBtnControl">[取消屏蔽]</span>'
  } else if (goodsStatus == '10') {
    return '<span data-menu-id="010102" data-click="downGoods(this);" class="selfBtnControl">[屏蔽]</span>'
  } else {
    return goodsStatus;
  }
}

function Num(ID) {
  return "<span class='Number'>" + ID + "</span>";
}

function toImg(url) {
  return "<img src='" + url + "' width='40' height='40'>";
}

//商品上架
function goodsPublish(e) {
  var gridTableList = $$selection.get("gridTable", "goodsList");
  var selectedRows = gridTableList.getSelectedRows();
  var self_id = $(e).parent().parent().find('.Number').text();
  $.confirm({
    title: "商品取消屏蔽上架确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认取消屏蔽该商品吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "取消屏蔽商品",
        btnClass: "btn-blue",
        action: function () {
          var arr = [];
          arr.push(self_id);
          $.doAjax("innerGoods/upGoods", {
            idStr: arr.join(",")
          }, function () {
            gridTableList.reloadData();
            $.success("取消屏蔽成功");
          }, function (error) {
            $.error(error.msg);
          });
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-default",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}
//商品下架
function downGoods(e) {
  var gridTableList = $$selection.get("gridTable", "goodsList");
  var selectedRows = gridTableList.getSelectedRows();
  var self_id = $(e).parent().parent().find('.Number').text();
  $.confirm({
    title: "商品屏蔽确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确认屏蔽该商品吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "屏蔽商品",
        btnClass: "btn-blue",
        action: function () {
          var arr = [];
          arr.push(self_id);
          $.doAjax("innerGoods/downGoods", {
            idStr: arr.join(",")
          }, function () {
            gridTableList.reloadData();
            $.success("屏蔽成功");
          }, function (error) {
            $.error(error.msg);
          });
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-default",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  })
}