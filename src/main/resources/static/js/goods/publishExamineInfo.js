// 批量发布商品
function submitGoods() {
  var gridTable = $$selection.get("gridTable", "goodsList");
  var selectedRows = gridTable.getSelectedRows();
  if (selectedRows.length === 0) {
    $.error("未选择任何数据");
    return;
  }
  selectedRows.forEach(function (row) {
    row.setStyleEdit();
  });
  var prisonsId = $("#prisonsId").val();
  $.confirm({
    title: "商品发布确认",
    icon: "fa fa-question-circle-o",
    content: "<p>确定将选中的 <span class='text-danger'>" + selectedRows.length + "</span> 条数据进行发布申请吗？</p>",
    theme: "modern",
    type: "orange",
    buttons: {
      ok: {
        text: "确定",
        btnClass: "btn-primary",
        action: function () {
          var arrHole = [];
          selectedRows.forEach(function (row) {
            var obj = {};
            obj.goodsId = row.getData().id;
            obj.goodsMarketPrice = row.getElement().find("input[name='goodsMarketPrice']").val();
            obj.goodsSellPrice = row.getElement().find("input[name='goodsSellPrice']").val();
            obj.goodsSettlementPrice = row.getElement().find("input[name='goodsSettlementPrice']").val();
            obj.prisonsId = prisonsId;
            arrHole.push(obj);
          });
          var json = JSON.stringify(arrHole);
          var scb = function () {
            $.success("提价发布商品审核成功：提交了" + selectedRows.length + "条数据");
            gridTable.reloadData();
          };
          var ecb = function (error) {
            $.error(error.msg);
            selectedRows.forEach(function (row) {
              row.unsetStyleEdit();
            });
          };
          $.doAjax("goodsPublish/subGoods", {
            "goodsJSON": json
          }, scb, ecb);
        }
      },
      cancel: {
        text: "取消",
        btnClass: "btn-success",
        action: function () {
          selectedRows.forEach(function (row) {
            row.unsetStyleEdit();
          });
        }
      }
    }
  });
}