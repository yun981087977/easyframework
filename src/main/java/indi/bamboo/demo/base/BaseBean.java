package indi.bamboo.demo.base;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.Date;

/**
 * 抽象基础Bean类，实现序列化接口
 * 包含公共基础属性
 * 所有实体对象都需要要继承该类
 *
 * @author 贾俊峰
 * @time 2017/6/16 16:38
 */
public abstract class BaseBean implements Serializable {

    private static final long serialVersionUID = -4657576302812093291L;

    /**
     * 实体类的主键
     * */
    private Long id;

    /**
     * 批量删除时用来接收主键id字符串
     * */
    @JSONField(serialize = false)
    private String delIdStr;

    /**
     * 创建人编号
     * */
    private Long createUser;

    /**
     * 创建人名称
     * */
    private String createUserName;

    /**
     * 更新人编号
     */
    private Long updateUser;

    /**
     * 更新人名称
     */
    private String updateUserName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    public BaseBean() {}

    public BaseBean(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDelIdStr() {
        return delIdStr;
    }

    public void setDelIdStr(String delIdStr) {
        this.delIdStr = delIdStr;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
