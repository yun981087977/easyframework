package indi.bamboo.demo.base;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 抽象分页基础类，所有需要分页的对象都需要继承该类。
 * 继承该类的对象，在进行list查询时会自动进行分页
 *
 * @author 贾俊峰
 * @time 2017/6/16 16:36
 */
public abstract class BasePagination extends BaseBean {

    private static final long serialVersionUID = -334273137692509998L;

    /**
     * 每页数据量
     */
    @JSONField(serialize = false)
    private Integer pageSize;

    /**
     * 当前页
     */
    @JSONField(serialize = false)
    private Integer pageNum;

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }
}
