package indi.bamboo.demo.base;

import java.io.Serializable;

/**
 * 抽象响应报文基础类
 *
 * @author 贾俊峰
 * @time 2017/6/19 16:02
 */
public abstract class BaseResponse implements Serializable {

    private static final long serialVersionUID = -4085475332893673259L;

    /**
     * 响应码
     */
    private Integer code;

    public BaseResponse(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
