package indi.bamboo.demo.base;

/**
 * 抽象异常基础类
 *
 * @author 贾俊峰
 * @time 2017/6/19 16:13
 */
public abstract class BaseException extends RuntimeException {

    private static final long serialVersionUID = 6002886481982958025L;

    /**
     * 异常码
     */
    private Integer code;

    /**
     * 异常信息
     */
    private String msg;

    /**
     * 获取异常信息
     * @return
     */
    @Override
    public abstract String getMessage();

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
