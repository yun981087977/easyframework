package indi.bamboo.demo.mapper;

import indi.bamboo.demo.base.BaseBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 基础dao层接口
 *
 * @author Libw
 * @time 2018/4/25 9:49
 */
public interface BaseMapper<T extends BaseBean>{

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    T selectById(Long id);

    /**
     * 查询单条记录
     * @param t
     * @return
     */
    T selectOne(@Param("item")T t);

    /**
     * 查询记录集合
     * @param t
     * @return
     */
    List<?> selectList(@Param("item")T t);

    /**
     * 通用的保存方法
     * @param t
     */
    void save(@Param("item")T t);

    /**
     * 批量保存
     * @param list
     */
    int batchSave(List<?> list);

    /**
     * 通用的修改方法
     * @param t
     */
    int update(@Param("item")T t);

    /**
     * 批量更新
     * @param list
     * @return
     */
    int batchUpdate(List<?> list);

    /**
     * 删除方法
     * @param id
     */
    int delById(Long id);

    /**
     * 批量删除
     * @param list
     * @return
     */
    int delList(List<?> list);
}
