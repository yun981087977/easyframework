package indi.bamboo.demo.exception;

import indi.bamboo.demo.base.BaseException;

/**
 * 应用异常：应用层的异常，主要是开发过程中可以预见的一些异常情况，如用户名/密码错误，参数类型不匹配等
 *
 * @author 贾俊峰
 * @time 2017/6/19 16:15
 */
public class AppException extends BaseException {

    private static final long serialVersionUID = -4273179126152391247L;

    public AppException(ErrorMessage errorMessage) {
        setCode(errorMessage.getCode());
        setMsg(errorMessage.getMsg());
    }
    public AppException(ErrorMessage errorMessage, Object... messages) {
        setCode(errorMessage.getCode());
        String msg = errorMessage.getMsg();
        for (Object message : messages) {
            msg = msg.replaceFirst("\\{0}", message.toString());
        }
        msg = msg.replaceAll("\\{0}", "{未定义}");
        setMsg(msg);
    }

    @Override
    public String getMessage() {
        return getMsg();
    }
}
