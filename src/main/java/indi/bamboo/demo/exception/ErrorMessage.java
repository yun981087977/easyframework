package indi.bamboo.demo.exception;

/**
 * 错误信息枚举
 *
 * @author 贾俊峰
 * @time 2017/6/19 16:18
 */
public enum ErrorMessage {

    /******* 系统(system) *******/
    SYSTEM_ERROR(1, "系统错误"),
    NETWORK_ERROR(2, "网络错误"),
    UNKNOWN_ERROR(3, "未知错误"),
    REQUEST_TYPE_ERROR(4, "请求类型错误"),
    REQUEST_PARAM_CAN_NOT_NULL(5, "缺少请求参数"),
    REQUEST_PARAM_TYPE_ERROR(6, "参数类型错误"),
    MODEL_CAN_NOT_FOUND(7, "传入参数查询不到对象"),
    DAO_BINDING_ERROR(8, "持久层绑定异常"),
    ENVIRONMENT_IS_NOT_DEFINED(9, "项目运行环境未定义"),
    CREATE_FILE_ERROR(10, "创建文件时出错"),
    INVALID_REQUEST_PATH(404, "无效的请求路径"),

    //========身份验证，权限类(auth) 1000
    ;

    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误信息
     */
    private String msg;

    ErrorMessage(final int code, final String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
