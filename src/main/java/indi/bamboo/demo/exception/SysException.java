package indi.bamboo.demo.exception;

import indi.bamboo.demo.base.BaseException;

/**
 * 系统异常：系统层的异常，主要为开发阶段无法预见，或无法处理的异常，如网络中段，支持层崩溃等
 *
 * @author 贾俊峰
 * @time 2017/6/19 16:29
 */
public class SysException extends BaseException {
    private static final long serialVersionUID = 2842004292345254271L;

    public SysException() {
        this(ErrorMessage.SYSTEM_ERROR);
    }
    public SysException(ErrorMessage errorMessage) {
        setCode(errorMessage.getCode());
        setMsg("系统错误：" + errorMessage.getMsg());
    }


    @Override
    public String getMessage() {
        return getMsg();
    }
}
