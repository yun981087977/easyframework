package indi.bamboo.demo.interceptor;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * package:    com.zoshine.wsyypt.interceptor
 * class_name: SecurityInterceptor
 * describe:   安全拦截器
 * Copyright:  Copyright (c) 2012-2018 All Rights Reserved.
 * Company:    zoshine.com Inc.
 * @author:   libw
 * @version:  1.0
 * creat_time: 2017/11/27 ,17:47
 */
public class SecurityInterceptor extends HandlerInterceptorAdapter {

	private Logger logger = LoggerFactory.getLogger(SecurityInterceptor.class);

	/**
	 * method_name: preHandle
	 * describe:   进入controller层之前判断是否拦截请求拦截请求
	 * @param:    request
	 * @param:    response
	 * @param:    o
	 * @author:   libw
	 * creat_time: 2017/11/27 , 17:43
	 * return:     boolean
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o)
		throws Exception {

		// 获取请求的url
		String url = request.getServletPath();
		logger.info("*********请求的url为：" + url);
		return true;
	}
}

