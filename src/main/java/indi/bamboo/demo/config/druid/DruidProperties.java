package indi.bamboo.demo.config.druid;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * package:    com.zoshine.wsyypt.config.druid
 * class_name: DruidProperties
 * describe:   druid配置的常用属性
 * Copyright:  Copyright (c) 2012-2018 All Rights Reserved.
 * Company:    zoshine.com Inc.
 * @author libw
 * @version 1.0
 * create_time: 2017/12/4 ,9:09
 */
@Configuration
@ConfigurationProperties(prefix = "druid")
public class DruidProperties {
    /**
     * 默认属性
     */
    private static final String DEFAULT_DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";
    private static final int DEFAULT_INITIAL_SIZE = 5;
    private static final int DEFAULT_MAX_ACTIVE = 20;
    private static final int DEFAULT_MIN_IDLE = 5;
    private static final long DEFAULT_MAX_WAIT = 60000;
    private static final boolean DEFAULT_POOL_PREPARED_STATEMENTS = false;
    private static final int DEFAULT_MAX_POOL_PREPARED_STATEMENT_PER_CONNECTION_SIZE = -1;
    private static final String DEFAULT_VALIDATION_QUERY = "SELECT 1";
    private static final int DEFAULT_VALIDATION_QUERY_TIMEOUT = 5;
    private static final boolean DEFAULT_TEST_ON_BORROW = false;
    private static final boolean DEFAULT_TEST_ON_RETURN = false;
    private static final boolean DEFAULT_TEST_WHILE_IDLE = true;
    private static final long DEFAULT_TIME_BETWEEN_EVICTION_RUNS_MILLIS = 60000;
    private static final long DEFAULT_MIN_EVICTABLE_IDLE_TIME_MILLIS = 300000;
    private static final String DEFAULT_CONNECTION_PROPERTIES = "druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000";
    private static final boolean DEFAULT_USE_GLOBAL_DATA_SOURCE_STAT = true;
    private static final String DEFAULT_FILTERS = "stat,wall,log4j";
    private static final boolean DEFAULT_MULTI_STATEMENT_ALLOW = false;

    private String driverClassName = DEFAULT_DRIVER_CLASS_NAME;
    private String url;
    private String username;
    private String password;

    /**
     * 初始化时建立物理连接的个数。初始化发生在显示调用init方法，或者第一次getConnection时
     */
    private int initialSize = DEFAULT_INITIAL_SIZE;
    /**
     * 最大连接数
     */
    private int maxActive = DEFAULT_MAX_ACTIVE;
    /**
     * 最小连接数
     */
    private int minIdle = DEFAULT_MIN_IDLE;
    /**
     * 获取连接时最大等待时间，单位毫秒。配置了maxWait之后，缺省启用公平锁，并发效率会有所下降，如果需要可以通过配置useUnfairLock属性为true使用非公平锁。
     */
    private long maxWait = DEFAULT_MAX_WAIT;
    /**
     * 是否缓存preparedStatement，也就是PSCache。PSCache对支持游标的数据库性能提升巨大，比如说oracle。在mysql下建议关闭。
     */
    private boolean poolPreparedStatements = DEFAULT_POOL_PREPARED_STATEMENTS;
    /**
     * 要启用PSCache，必须配置大于0，当大于0时，poolPreparedStatements自动触发修改为true。在Druid中，不会存在Oracle下PSCache占用内存过多的问题，可以把这个数值配置大一些，比如说100
     */
    private int maxPoolPreparedStatementPerConnectionSize = DEFAULT_MAX_POOL_PREPARED_STATEMENT_PER_CONNECTION_SIZE;
    /**
     * 用来检测连接是否有效的sql，要求是一个查询语句，常用select 'x'。如果validationQuery为null，testOnBorrow、testOnReturn、testWhileIdle都不会其作用。
     */
    private String validationQuery = DEFAULT_VALIDATION_QUERY;
    /**
     * 单位：秒，检测连接是否有效的超时时间。底层调用jdbc Statement对象的void setQueryTimeout(int seconds)方法
     */
    private int validationQueryTimeout = DEFAULT_VALIDATION_QUERY_TIMEOUT;
    /**
     * 申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能。
     */
    private boolean testOnBorrow = DEFAULT_TEST_ON_BORROW;
    /**
     * 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能。
     */
    private boolean testOnReturn = DEFAULT_TEST_ON_RETURN;
    /**
     * 建议配置为true，不影响性能，并且保证安全性。申请连接的时候检测，如果空闲时间大于timeBetweenEvictionRunsMillis，执行validationQuery检测连接是否有效。
     */
    private boolean testWhileIdle = DEFAULT_TEST_WHILE_IDLE;
    /**
     * 有两个含义：1) Destroy线程会检测连接的间隔时间，如果连接空闲时间大于等于minEvictableIdleTimeMillis则关闭物理连接。2) testWhileIdle的判断依据，详细看testWhileIdle属性的说明
     */
    private long timeBetweenEvictionRunsMillis = DEFAULT_TIME_BETWEEN_EVICTION_RUNS_MILLIS;
    /**
     * 连接保持空闲而不被驱逐的最长时间
     */
    private long minEvictableIdleTimeMillis = DEFAULT_MIN_EVICTABLE_IDLE_TIME_MILLIS;
    /**
     * 通过connectProperties属性来打开mergeSql功能；慢SQL记录
     */
    private String connectionProperties = DEFAULT_CONNECTION_PROPERTIES;
    /**
     * 合并多个DruidDataSource的监控数据
     */
    private boolean useGlobalDataSourceStat = DEFAULT_USE_GLOBAL_DATA_SOURCE_STAT;
    /**
     * 监控统计拦截的filters，去掉后监控界面sql无法统计，'wall'用于防火墙
     */
    private String filters = DEFAULT_FILTERS;
    /**
     * 是否允许批量提交
     */
    private boolean multiStatementAllow = DEFAULT_MULTI_STATEMENT_ALLOW; 

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public int getMaxActive() {
        return maxActive;
    }

    public void setMaxActive(int maxActive) {
        this.maxActive = maxActive;
    }

    public int getMinIdle() {
        return minIdle;
    }

    public void setMinIdle(int minIdle) {
        this.minIdle = minIdle;
    }

    public int getInitialSize() {
        return initialSize;
    }

    public void setInitialSize(int initialSize) {
        this.initialSize = initialSize;
    }

    public boolean isTestOnBorrow() {
        return testOnBorrow;
    }

    public void setTestOnBorrow(boolean testOnBorrow) {
        this.testOnBorrow = testOnBorrow;
    }

    public long getMaxWait() {
        return maxWait;
    }

    public void setMaxWait(long maxWait) {
        this.maxWait = maxWait;
    }

    public boolean isPoolPreparedStatements() {
        return poolPreparedStatements;
    }

    public void setPoolPreparedStatements(boolean poolPreparedStatements) {
        this.poolPreparedStatements = poolPreparedStatements;
    }

    public int getMaxPoolPreparedStatementPerConnectionSize() {
        return maxPoolPreparedStatementPerConnectionSize;
    }

    public void setMaxPoolPreparedStatementPerConnectionSize(int maxPoolPreparedStatementPerConnectionSize) {
        this.maxPoolPreparedStatementPerConnectionSize = maxPoolPreparedStatementPerConnectionSize;
    }

    public String getValidationQuery() {
        return validationQuery;
    }

    public void setValidationQuery(String validationQuery) {
        this.validationQuery = validationQuery;
    }

    public int getValidationQueryTimeout() {
        return validationQueryTimeout;
    }

    public void setValidationQueryTimeout(int validationQueryTimeout) {
        this.validationQueryTimeout = validationQueryTimeout;
    }

    public boolean isTestOnReturn() {
        return testOnReturn;
    }

    public void setTestOnReturn(boolean testOnReturn) {
        this.testOnReturn = testOnReturn;
    }

    public boolean isTestWhileIdle() {
        return testWhileIdle;
    }

    public void setTestWhileIdle(boolean testWhileIdle) {
        this.testWhileIdle = testWhileIdle;
    }

    public long getTimeBetweenEvictionRunsMillis() {
        return timeBetweenEvictionRunsMillis;
    }

    public void setTimeBetweenEvictionRunsMillis(long timeBetweenEvictionRunsMillis) {
        this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
    }

    public long getMinEvictableIdleTimeMillis() {
        return minEvictableIdleTimeMillis;
    }

    public void setMinEvictableIdleTimeMillis(long minEvictableIdleTimeMillis) {
        this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
    }

    public String getConnectionProperties() {
        return connectionProperties;
    }

    public void setConnectionProperties(String connectionProperties) {
        this.connectionProperties = connectionProperties;
    }

    public boolean isUseGlobalDataSourceStat() {
        return useGlobalDataSourceStat;
    }

    public void setUseGlobalDataSourceStat(boolean useGlobalDataSourceStat) {
        this.useGlobalDataSourceStat = useGlobalDataSourceStat;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    public boolean isMultiStatementAllow() {
        return multiStatementAllow;
    }

    public void setMultiStatementAllow(boolean multiStatementAllow) {
        this.multiStatementAllow = multiStatementAllow;
    }
}
