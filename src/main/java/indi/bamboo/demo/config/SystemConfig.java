package indi.bamboo.demo.config;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Properties;

/**
 * @author wangzx
 * @version V1.0
 * @Package com.zoshine.wsyypt.util
 * @Description: 适合自定义的配置信息，spring 容器默认的配置信息会读不到
 * @date 2017/11/13 13:32
 */
@Component
public class SystemConfig {

    private static Properties props ;

    public SystemConfig(){

        try {
            Resource resource = new ClassPathResource("config/application.properties");
            props = PropertiesLoaderUtils.loadProperties(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 获取属性
     * @param key
     * @return
     */
    public static String getProperty(String key){
        return props == null ? null :  props.getProperty(key);
    }

    /**
     * 获取属性
     * @param key 属性key
     * @param defaultValue 属性value
     * @return
     */
    public static String getProperty(String key,String defaultValue){
        return props == null ? null : props.getProperty(key, defaultValue);
    }

    /**
     * 获取properyies属性
     * @return
     */
    public static Properties getProperties(){
        return props;
    }

}
