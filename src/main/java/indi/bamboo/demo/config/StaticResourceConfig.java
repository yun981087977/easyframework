package indi.bamboo.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @ClassName: StaticResourceConfig
 * @Description: TODO
 * @Author: Bamboo
 * @Date: 2018/11/1313:50
 * @Version: 1.0
 */
@Configuration
@EnableWebMvc
public interface StaticResourceConfig extends WebMvcConfigurer {
    @Override
    default void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
    }
}