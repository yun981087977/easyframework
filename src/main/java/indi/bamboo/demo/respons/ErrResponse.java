package indi.bamboo.demo.respons;

import indi.bamboo.demo.base.BaseResponse;
import indi.bamboo.demo.base.BaseException;
import indi.bamboo.demo.exception.ErrorMessage;

/**
 * 异常响应报文
 *
 * @author 贾俊峰
 * @time 2017/6/19 16:02
 */
public class ErrResponse extends BaseResponse {
    private static final long serialVersionUID = 7418027350988352830L;

    /**错误信息*/
    private String msg;

    public ErrResponse(ErrorMessage errorMessage) {
        this(errorMessage.getCode(), errorMessage.getMsg());
    }
    public ErrResponse(BaseException exception) {
        this(exception.getCode(), exception.getMsg());
    }
    public ErrResponse(Integer code, String msg) {
        super(code);
        this.msg = msg;
    }
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
