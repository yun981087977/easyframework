package indi.bamboo.demo.respons;

import indi.bamboo.demo.base.BaseResponse;

/**
 * 应用响应报文
 *
 * @author 贾俊峰
 * @time 2017/6/19 16:01
 */
public class AppResponse extends BaseResponse {
    private static final long serialVersionUID = 532848992064945547L;

    /** 响应对象，可以是任意数据 */
    private Object data;

    /** 成功响应报文 */
    public static AppResponse OK = new AppResponse(null);

    public AppResponse(Object data) {
        super(0);
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
