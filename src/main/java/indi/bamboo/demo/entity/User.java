package indi.bamboo.demo.entity;

import indi.bamboo.demo.base.BasePagination;

public class User extends BasePagination {
    /**
     * 用户名
     */
    private String userNm;

    /**
     * 密码
     */
    private String password;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 获取用户名
     *
     * @return user_nm - 用户名
     */
    public String getUserNm() {
        return userNm;
    }

    /**
     * 设置用户名
     *
     * @param userNm 用户名
     */
    public void setUserNm(String userNm) {
        this.userNm = userNm == null ? null : userNm.trim();
    }

    /**
     * 获取密码
     *
     * @return password - 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     *
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * 获取手机号
     *
     * @return mobile - 手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机号
     *
     * @param mobile 手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }
}