package indi.bamboo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @ClassName: HelloWorldController
 * @Description: 测试用
 * @Author: Bamboo
 * @Date: 2018/11/1313:18
 * @Version: 1.0
 */
@Controller
@RequestMapping("/hello")
public class HelloWorldController {

    @RequestMapping("index")
    public String index(){
        return "/html/index.html";
    }
}
