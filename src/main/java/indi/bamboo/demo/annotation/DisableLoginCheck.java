package indi.bamboo.demo.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义注解：禁用登录检查
 * 添加了该注解的Controller类或方法在被调用时不会作登录检查
 * @author 贾俊峰
 * @time 2017/7/21 09:48
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DisableLoginCheck {
}
